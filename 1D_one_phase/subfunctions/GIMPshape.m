function [N,dN]=GIMPshape(xp,xn,dcell,dparticle)   


L  = dcell;
lp = dparticle/2;
dx = xp - xn;

if dx >= -L-lp && dx <= -L+lp
    N = (L+lp+dx).^2/(4*L*lp);
    dN = (L+lp+dx)/(2*L*lp);
    
elseif dx > -L+lp && dx <= -lp
    N = 1 + dx/L;
    dN = 1/L;
    
elseif dx > -lp && dx <= lp
    N = 1 - (dx.^2+lp.^2)/(2*L*lp);
    dN = -dx/L/lp;
    
elseif dx > lp && dx <= L-lp
    N = 1-dx/L;
    dN = -1/L;
    
elseif dx > L-lp && dx <= L+lp
    N = (L+lp-dx).^2/(4*L*lp);
    dN = -(L+lp-dx)/(2*L*lp);
    
else
    N = 0;
    dN = 0;
end  
    end