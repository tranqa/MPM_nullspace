function [N,dN]=Bsplineshape(xp,xn,dcell)     

L  = dcell;
dx = xp - xn;

if dx >= -2*L && dx <= -L
    N = 1/6/L^3*dx^3+1/L^2*dx^2+2*dx/L+4/3;
    dN = 1/2/L^3*dx^2+2/L^2*dx+2/L;
    
elseif dx > -L && dx <= 0
    N = -1/2/L^3*dx^3-1/L^2*dx^2+2/3;
    dN = -3/2/L^3*dx^2-2/L^2*dx;
    
elseif dx > 0 && dx <= L
    N = 1/2/L^3*dx^3-1/L^2*dx^2+2/3;
    dN = 3/2/L^3*dx^2-2/L^2*dx;
    
elseif dx > L && dx <= 2*L
    N = -1/6/L^3*dx^3+1/L^2*dx^2-2*dx/L+4/3;
    dN = -1/2/L^3*dx^2+2/L^2*dx-2/L;
    
else
    N = 0;
    dN = 0;
end  

    end