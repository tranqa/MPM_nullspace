function [ui,ri] = filter1(dg,S)

%ri nullspace component and ui nonnullspace component

[~, Si, V] = svd(S);
k=rank(Si);
ui=0;
for i=1:k
    ui = ui + dot(dg',V(:,i))*V(:,i);
    ri=dg-ui;
end
end