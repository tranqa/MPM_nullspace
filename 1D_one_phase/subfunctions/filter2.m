function [ui] = filter2(dg,S,kk)

[Q1, ~] = qr(S,0);
ui=0;
for i=1:kk
    ui = ui + dot(dg',Q1(:,i))*Q1(:,i);
end    
end