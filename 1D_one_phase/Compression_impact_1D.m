clear all;
tic;

addpath('subfunctions');


% Computational grid ******************************************************
choice=3;                                 
% Choice of version 1=MPM 2=GIMP 3=DDMP 4 =splines
cfilter=3;                                
% Choice of null space filter 1 = House Holder, 2= SVD decomposition, 3=QR
% decomposition

L=1.012;                                  % total length
rho=1.0;                                  % density
E=10000.0;                                % modulus
c=sqrt(E/rho);                            % wave speed
g=0;
Np=0;
u=0;
elemCount = 500 + 6;                      % total number of elements  
particle_per_element = 2.0;
nodes = linspace(0.0,L,elemCount+1);      % list of nodes number (node coordinate) (0-13)

elements = zeros(elemCount,2.0);          % zero matrix to store begin and end nodes of each element
for ie =1.0:elemCount
elements(ie,:) = ie:ie+1;                 % Store begin and end nodes of each element
end

nodeCount = size(nodes,2);                          % count node in elements
deltax = L/(particle_per_element*elemCount);        % size of particle
deltal = L/elemCount;                               % nodal spacing

%% define time factor*******************************************************
t = 0.0;
ftime = 0.75*(L-6*deltal)/c;
dtime = 0.001*(L-6*deltal)/c;

% time integration
p_b = 0.5;
a_m = (2*p_b-1)/(1+p_b);
beta = (5-3*p_b)/(1+p_b).^2/(2-p_b);
gamma = 3/2-a_m;

% Data store
stress=[];
time=[];
check=[];

%% Material points **********************************************************
% Initial conditions
xp = zeros(particle_per_element*(elemCount-6),1);           % zero matrix for material points positions
pCount = length(xp);                                        % total number of material points

for p=1:pCount
    xp(p)=3*deltal + 0.5*deltax+(p-1)*deltax;
end

xo=xp;

Vp = deltax * ones(pCount,1);           % volume
Mp = rho * deltax * ones(pCount,1);     % mass ( rho: density)
Fp = ones(pCount,1);                    % gradient deformation
Lp = zeros(pCount,1);
Lp1 = zeros(pCount,1);
Lp2= zeros(pCount,1);
dEps = zeros(pCount,1);
dEps1 = zeros(pCount,1);
dEps2 = zeros(pCount,1);
ep = zeros(pCount,1); 
ep1 = zeros(pCount,1); 
ep2 = zeros(pCount,1); 
dp = zeros(pCount,1); 
Vp0 = Vp;                               % initial volume
sp = zeros(pCount,1);                   % stress
sp_sm = zeros(pCount,1);                % stress
vp = zeros(pCount,1);                   % zero matrix for material points velocities
ptraction = zeros(pCount,1); 

%% nodal quantities*********************************************************
nmass = zeros(nodeCount,1);             % nodal mass vector
nmomentum = zeros(nodeCount,1);         % nodal momentum vector
nvolume = zeros(nodeCount,1);
niforce = zeros(nodeCount,1);           % nodal internal force vector
neforce = zeros(nodeCount,1);           % nodal external force vector
eforce = zeros(nodeCount,1);            % nodal total force vector
nvelo = zeros(nodeCount,1);             % nodal velocity vector
nvelol = zeros(nodeCount,1);            % nodal velocity vector
nvelo_old = zeros(nodeCount,1);         % nodal velocity vector
nvelo_aver = zeros(nodeCount,1);        % nodal velocity vector
ntraction = zeros(nodeCount,1); 

nstress  = zeros(nodeCount,1);    
A  = zeros(nodeCount,1);  
nacc = zeros(nodeCount,1);
nacc_old = zeros(nodeCount,1);
nacc_middle = zeros(nodeCount,1);

%% start the algorithm******************************************************
while (t<=ftime)
    t
%% Reset value of node
nmass(:) = 0; 
nmomentum(:) = 0; 
niforce(:)=0;
% neforce(:)=0;
nvelo_old = nvelo;
nvelo(:)=0;
nvelol(:)=0;
nacc(:)=0;
nacc_middle(:)=0;
nstress(:)=0;
nvolume(:)=0;
ntraction(:)=0;
ptraction(:)=0;

%% Structure Mesh interaction***********************************************
mpoints=cell(elemCount,1);              % matrix to store particles for each elements

% loop for particles and define the position of particles in elements
for p=1:pCount
x = xp(p);
e=(floor(x/deltal)+1);
pElems(p) = e;                          % Particle p stay in element e
end

% loop over elements and store particles in element "e"
for e=1:elemCount
    id = find(pElems==e);
    mpoints{e}=id;                      % mpoints {e} indicates particles in e
end

%% Boundary
if t < 0.5*(L-6*deltal)/c;
ptraction(pCount) = -1;
ptraction(pCount-1) = -1;
end

%% particle to node
for e =1:elemCount                      % loop over computational elements
        
esctr = elements(e,:);                  % nodes of element "e"
enode = nodes(esctr);                   % nodal coordinates
mpts = mpoints{e};                      % particles in element "e"

% Shape function
for p=1:length(mpts)                    % loop over particles inside 1 element
    
    pid = mpts(p);
    if choice == 1
    [N1,N2,dN1,dN2]=linearshape(xp(pid),enode(1),enode(2)); 
    N0=0;dN0=0;N3=0;dN3=0;
    elseif choice == 2
    [N0,dN0]=GIMPshape(xp(pid),enode(1)-deltal,deltal,Vp(pid));
    [N1,dN1]=GIMPshape(xp(pid),enode(1),deltal,Vp(pid));
    [N2,dN2]=GIMPshape(xp(pid),enode(2),deltal,Vp(pid));
    [N3,dN3]=GIMPshape(xp(pid),enode(2)+deltal,deltal,Vp(pid)); 
    elseif choice==3
    [N1,N2,dN1,dN2,alpha]=DDMPshape(xp(pid),enode(1),enode(2));
    N0=0;dN0=0;N3=0;dN3=0;
    elseif choice == 4
    [N0,dN0]=Bsplineshape(xp(pid),enode(1)-deltal,deltal);
    [N1,dN1]=Bsplineshape(xp(pid),enode(1),deltal);
    [N2,dN2]=Bsplineshape(xp(pid),enode(2),deltal);
    [N3,dN3]=Bsplineshape(xp(pid),enode(2)+deltal,deltal); 
    end
    
% particle mass and momentum to node***************************************
nmass(esctr(1)-1)     = nmass(esctr(1)-1) + N0*Mp(pid);
nmass(esctr(1))     = nmass(esctr(1)) + N1*Mp(pid);
nmass(esctr(2))     = nmass(esctr(2)) + N2*Mp(pid); 
nmass(esctr(2)+1)     = nmass(esctr(2)+1) + N3*Mp(pid);

nmomentum(esctr(1)-1) = nmomentum(esctr(1)-1) + N0*Mp(pid)*vp(pid);    
nmomentum(esctr(1)) = nmomentum(esctr(1)) + N1*Mp(pid)*vp(pid);
nmomentum(esctr(2)) = nmomentum(esctr(2)) + N2*Mp(pid)*vp(pid);
nmomentum(esctr(2)+1) = nmomentum(esctr(2)+1) + N3*Mp(pid)*vp(pid);

nvolume(esctr(1)-1) = nvolume(esctr(1)-1) + N0*Vp(pid);    
nvolume(esctr(1)) = nvolume(esctr(1)) + N1*Vp(pid);
nvolume(esctr(2)) = nvolume(esctr(2)) + N2*Vp(pid);
nvolume(esctr(2)+1) = nvolume(esctr(2)+1) + N3*Vp(pid);

end
end

%% nodal stress
if choice==3
   for e =1:elemCount                   % loop over computational elements
        
esctr = elements(e,:);                  % nodes of element "e"
enode = nodes(esctr);                   % nodal coordinates
mpts = mpoints{e};                      % particles in element "e"

% Shape function
for p=1:length(mpts)                    % loop over particles inside 1 element
     pid = mpts(p);
    [N1,N2,dN1,dN2,alpha]=DDMPshape(xp(pid),enode(1),enode(2));
    N0=0;dN0=0;N3=0;dN3=0;
    
    nstress(esctr(1)-1) = nstress(esctr(1)-1) + (1-alpha) * Vp(pid)*sp(pid)*N0;
    nstress(esctr(1))   = nstress(esctr(1)) + (1-alpha) * Vp(pid)*sp(pid)*N1;
    nstress(esctr(2))   = nstress(esctr(2)) + (1-alpha) * Vp(pid)*sp(pid)*N2;
    nstress(esctr(2)+1) = nstress(esctr(2)+1) + (1-alpha) * Vp(pid)*sp(pid)*N3;
   
end
end
end

for n = 4:nodeCount-3
    nstress(n) = nstress(n)/nvolume(n);
end


%% internal force
for e =1:elemCount                      % loop over computational elements
        
esctr = elements(e,:);                  % nodes of element "e"
enode = nodes(esctr);                   % nodal coordinates
mpts = mpoints{e};                      % particles in element "e"

% Shape function
for p=1:length(mpts)                    % loop over particles inside 1 element
    
    pid = mpts(p);
    if choice == 1
    [N1,N2,dN1,dN2]=linearshape(xp(pid),enode(1),enode(2)); 
    N0=0;dN0=0;N3=0;dN3=0;
    elseif choice == 2
    [N0,dN0]=GIMPshape(xp(pid),enode(1)-deltal,deltal,Vp(pid));
    [N1,dN1]=GIMPshape(xp(pid),enode(1),deltal,Vp(pid));
    [N2,dN2]=GIMPshape(xp(pid),enode(2),deltal,Vp(pid));
    [N3,dN3]=GIMPshape(xp(pid),enode(2)+deltal,deltal,Vp(pid)); 
    elseif choice==3
    [N1,N2,dN1,dN2,alpha]=DDMPshape(xp(pid),enode(1),enode(2));
    N0=0;dN0=0;N3=0;dN3=0;
    elseif choice == 4
    [N0,dN0]=Bsplineshape(xp(pid),enode(1)-deltal,deltal);
    [N1,dN1]=Bsplineshape(xp(pid),enode(1),deltal);
    [N2,dN2]=Bsplineshape(xp(pid),enode(2),deltal);
    [N3,dN3]=Bsplineshape(xp(pid),enode(2)+deltal,deltal); 
    end
% internal force******************************************************<****
if choice==3

niforce(esctr(1)) = niforce(esctr(1)) - Vp(pid)*sp(pid)*dN1*alpha;
niforce(esctr(2)) = niforce(esctr(2)) - Vp(pid)*sp(pid)*dN2*alpha;

else
niforce(esctr(1)-1) = niforce(esctr(1)-1) - Vp(pid)*sp(pid)*dN0;
niforce(esctr(1)) = niforce(esctr(1)) - Vp(pid)*sp(pid)*dN1;
niforce(esctr(2)) = niforce(esctr(2)) - Vp(pid)*sp(pid)*dN2;
niforce(esctr(2)+1) = niforce(esctr(2)+1) - Vp(pid)*sp(pid)*dN3;
end

% internal force******************************************************<****
neforce(esctr(1)-1) = neforce(esctr(1)-1) - g*Vp(pid)*Mp(pid)*N0;
neforce(esctr(1)) = neforce(esctr(1)) - g*Vp(pid)*Mp(pid)*N1;
neforce(esctr(2)) = neforce(esctr(2)) - g*Vp(pid)*Mp(pid)*N2;
neforce(esctr(2)+1) = neforce(esctr(2)+1) - g*Vp(pid)*Mp(pid)*N3;

ntraction(esctr(1)-1) = ntraction(esctr(1)-1) - Vp(pid)*ptraction(pid)*N0/deltal;
ntraction(esctr(1)) = ntraction(esctr(1)) - Vp(pid)*ptraction(pid)*N1/deltal;
ntraction(esctr(2)) = ntraction(esctr(2)) - Vp(pid)*ptraction(pid)*N2/deltal;
ntraction(esctr(2)+1) = ntraction(esctr(2)+1) - Vp(pid)*ptraction(pid)*N3/deltal;
end
end

for n=4:nodeCount-3
    if n==4
        niforce(n) = niforce(n) - (-nstress(n)-nstress(n+1))/2;
    elseif n==nodeCount-3
        niforce(n) = niforce(n) - (nstress(n-1)+nstress(n))/2;
    else
        niforce(n) = niforce(n) - (nstress(n-1)-nstress(n+1))/2;
    end
end

nforce = niforce + neforce + ntraction;

%% update nodal momenta*****************************************************
nmomentum(1) = 0; % Boundary conditions
nforce(1) = 0;
nmomentum(2) = 0; % Boundary conditions
nforce(2) = 0;
nmomentum(3) = 0; % Boundary conditions
nforce(3) = 0;
nmomentum(4) = 0; % Boundary conditions
nforce(4) = 0;

for n=4:nodeCount-3
nmomentum(n) = nmomentum(n) + nforce(n)*dtime;
nacc_middle(n) = nforce(n)/nmass(n);
% nvelol(n) = nvelo(n) + nacc(n)*dtime;
nacc(n) = (nacc_middle(n) - a_m*nacc_old(n))/(1-a_m);
end

%% update particle velocity 
     % loop over elements and store particles in element "e"
for e =1:elemCount     
        esctr = elements(e,:);          
        enode = nodes(esctr);                 
        mpts = mpoints{e};              

% loop over particles
    for p=1:length (mpts)
        pid = mpts(p);                  % loop each particle (pid) inside an element
        
% compute shape functions N1, N2
    if choice ==1
    [N1,N2,dN1,dN2]=linearshape(xp(pid),enode(1),enode(2));
    N0=0;dN0=0;N3=0;dN3=0;
    elseif choice == 2
    [N0,dN0]=GIMPshape(xp(pid),enode(1)-deltal,deltal,Vp(pid));
    [N1,dN1]=GIMPshape(xp(pid),enode(1),deltal,Vp(pid));
    [N2,dN2]=GIMPshape(xp(pid),enode(2),deltal,Vp(pid));
    [N3,dN3]=GIMPshape(xp(pid),enode(2)+deltal,deltal,Vp(pid));  
    elseif choice==3
    [N1,N2,dN1,dN2,alpha]=DDMPshape(xp(pid),enode(1),enode(2));
    N0=0;dN0=0;N3=0;dN3=0;
    elseif choice == 4
    [N0,dN0]=Bsplineshape(xp(pid),enode(1)-deltal,deltal);
    [N1,dN1]=Bsplineshape(xp(pid),enode(1),deltal);
    [N2,dN2]=Bsplineshape(xp(pid),enode(2),deltal);
    [N3,dN3]=Bsplineshape(xp(pid),enode(2)+deltal,deltal);     
    end
if cfilter~=0 && (choice==3||choice==1)
    
if esctr(1)==4
    %Update particle velocity and position
    vp(pid) = vp(pid) + dtime * (N1*((1-gamma)*nacc_old(esctr(1))+gamma*nacc(esctr(1))) + N2*((1-gamma)*nacc_old(esctr(2))+gamma*nacc(esctr(2))) + N3*((1-gamma)*nacc_old(esctr(2)+1)+gamma*nacc(esctr(2)+1)));
    xp(pid) = xp(pid) + dtime * (N1*(nmomentum(esctr(1))/nmass(esctr(1))+dtime*((0.5-beta)*nacc_old(esctr(1))+beta*nacc(esctr(1))))+ N2*(nmomentum(esctr(2))/nmass(esctr(2))+dtime*((0.5-beta)*nacc_old(esctr(2))+beta*nacc(esctr(2))))+ N3*(nmomentum(esctr(2)+1)/nmass(esctr(2)+1)+dtime*((0.5-beta)*nacc_old(esctr(2)+1)+beta*nacc(esctr(2)+1))));
elseif esctr(2)==nodeCount-3
    %Update particle velocity and position
    vp(pid) = vp(pid) + dtime * (N0*((1-gamma)*nacc_old(esctr(1)-1)+gamma*nacc(esctr(1)-1)) + N1*((1-gamma)*nacc_old(esctr(1))+gamma*nacc(esctr(1))) + N2*((1-gamma)*nacc_old(esctr(2))+gamma*nacc(esctr(2))));
    xp(pid) = xp(pid) + dtime * (N0*(nmomentum(esctr(1)-1)/nmass(esctr(1)-1)+dtime*((0.5-beta)*nacc_old(esctr(1)-1)+beta*nacc(esctr(1)-1))) + N1*(nmomentum(esctr(1))/nmass(esctr(1))+dtime*((0.5-beta)*nacc_old(esctr(1))+beta*nacc(esctr(1))))+ N2*(nmomentum(esctr(2))/nmass(esctr(2))+dtime*((0.5-beta)*nacc_old(esctr(2))+beta*nacc(esctr(2)))));
else
     %Update particle velocity and position
    vp(pid) = vp(pid) + dtime * (N0*((1-gamma)*nacc_old(esctr(1)-1)+gamma*nacc(esctr(1)-1)) + N1*((1-gamma)*nacc_old(esctr(1))+gamma*nacc(esctr(1))) + N2*((1-gamma)*nacc_old(esctr(2))+gamma*nacc(esctr(2))) + N3*((1-gamma)*nacc_old(esctr(2)+1)+gamma*nacc(esctr(2)+1)));
    xp(pid) = xp(pid) + dtime * (N0*(nmomentum(esctr(1)-1)/nmass(esctr(1)-1)+dtime*((0.5-beta)*nacc_old(esctr(1)-1)+beta*nacc(esctr(1)-1))) + N1*(nmomentum(esctr(1))/nmass(esctr(1))+dtime*((0.5-beta)*nacc_old(esctr(1))+beta*nacc(esctr(1))))+ N2*(nmomentum(esctr(2))/nmass(esctr(2))+dtime*((0.5-beta)*nacc_old(esctr(2))+beta*nacc(esctr(2))))+ N3*(nmomentum(esctr(2)+1)/nmass(esctr(2)+1)+dtime*((0.5-beta)*nacc_old(esctr(2)+1)+beta*nacc(esctr(2)+1))));
end
   else
if esctr(1)==4
    %Update particle velocity and position
    vp(pid) = vp(pid) + dtime * (N1*nacc_middle(esctr(1)) + N2*nacc_middle(esctr(2)) + N3*nacc_middle(esctr(2)+1));
    xp(pid) = xp(pid) + dtime * (N1*(nmomentum(esctr(1))/nmass(esctr(1)))+ N2*(nmomentum(esctr(2))/nmass(esctr(2)))+ N3*(nmomentum(esctr(2)+1)/nmass(esctr(2)+1)));
    dp(pid) = dp(pid) + dtime * (N1*(nmomentum(esctr(1))/nmass(esctr(1)))+ N2*(nmomentum(esctr(2))/nmass(esctr(2)))+ N3*(nmomentum(esctr(2)+1)/nmass(esctr(2)+1)));

elseif esctr(2)==nodeCount-3
    
    %Update particle velocity and position
    vp(pid) = vp(pid) + dtime * (N0*nacc_middle(esctr(1)-1) + N1*nacc_middle(esctr(1)) + N2*nacc_middle(esctr(2)));
    xp(pid) = xp(pid) + dtime * (N0*(nmomentum(esctr(1)-1)/nmass(esctr(1)-1)) + N1*(nmomentum(esctr(1))/nmass(esctr(1)))+ N2*(nmomentum(esctr(2))/nmass(esctr(2))));
    dp(pid) = dp(pid) + dtime * (N0*(nmomentum(esctr(1)-1)/nmass(esctr(1)-1)) + N1*(nmomentum(esctr(1))/nmass(esctr(1)))+ N2*(nmomentum(esctr(2))/nmass(esctr(2))));

else
     %Update particle velocity and position
    vp(pid) = vp(pid) + dtime * (N0*nacc_middle(esctr(1)-1) + N1*nacc_middle(esctr(1)) + N2*nacc_middle(esctr(2)) + N3*nacc_middle(esctr(2)+1));
    xp(pid) = xp(pid) + dtime * (N0*(nmomentum(esctr(1)-1)/nmass(esctr(1)-1)) + N1*(nmomentum(esctr(1))/nmass(esctr(1)))+ N2*(nmomentum(esctr(2))/nmass(esctr(2)))+ N3*(nmomentum(esctr(2)+1)/nmass(esctr(2)+1)));
    dp(pid) = dp(pid) + dtime * (N0*(nmomentum(esctr(1)-1)/nmass(esctr(1)-1)) + N1*(nmomentum(esctr(1))/nmass(esctr(1)))+ N2*(nmomentum(esctr(2))/nmass(esctr(2)))+ N3*(nmomentum(esctr(2)+1)/nmass(esctr(2)+1)));
end
    end
    
    end
end

    %% Structure Mesh interaction***********************************************
mpoints=cell(elemCount,1);              % matrix to store particles for each elements

% loop for particles and define the position of particles in elements
for p=1:pCount
x = xp(p);
e=(floor(x/deltal)+1);
pElems(p) = e;                          % Particle p stay in element e
end

% loop over elements and store particles in element "e"
for e=1:elemCount
    id = find(pElems==e);
    mpoints{e}=id;                      % mpoints {e} indicates particles in e
end

    %% and recompute nodal velocities
    
    for e =1:elemCount                      % loop over computational elements
        
esctr = elements(e,:);                  % nodes of element "e"
enode = nodes(esctr);                   % nodal coordinates
mpts = mpoints{e};                      % particles in element "e"

% Shape function
for p=1:length(mpts)                    % loop over particles inside 1 element
    
    pid = mpts(p);
    
    if choice == 1
    [N1,N2,dN1,dN2]=linearshape(xp(pid),enode(1),enode(2));
    N0=0;dN0=0;N3=0;dN3=0;
    elseif choice == 2
    [N0,dN0]=GIMPshape(xp(pid),enode(1)-deltal,deltal,Vp(pid));
    [N1,dN1]=GIMPshape(xp(pid),enode(1),deltal,Vp(pid));
    [N2,dN2]=GIMPshape(xp(pid),enode(2),deltal,Vp(pid));
    [N3,dN3]=GIMPshape(xp(pid),enode(2)+deltal,deltal,Vp(pid));   
    elseif choice==3
    [N1,N2,dN1,dN2,alpha]=DDMPshape(xp(pid),enode(1),enode(2));
    N0=0;dN0=0;N3=0;dN3=0;
    elseif choice == 4
    [N0,dN0]=Bsplineshape(xp(pid),enode(1)-deltal,deltal);
    [N1,dN1]=Bsplineshape(xp(pid),enode(1),deltal);
    [N2,dN2]=Bsplineshape(xp(pid),enode(2),deltal);
    [N3,dN3]=Bsplineshape(xp(pid),enode(2)+deltal,deltal);     
    end

% particle (updated) velocities are mapped back to nodes
    nvelo(esctr(1)-1) = nvelo(esctr(1)-1)+ N0 * Mp(pid) * vp(pid);
    nvelo(esctr(1)) = nvelo(esctr(1))+ N1 * Mp(pid) * vp(pid);
    nvelo(esctr(2)) = nvelo(esctr(2))+ N2 * Mp(pid) * vp(pid);
    nvelo(esctr(2)+1) = nvelo(esctr(2)+1)+ N3 * Mp(pid) * vp(pid);

end
end

% compute nodal velocities
    for n = 4:nodeCount-3
    nvelo(n) = nvelo(n)/nmass(n);  
    end
    
    nvelo(1) = 0;                       % boundary conditions
    nvelo(2) = 0;                       % boundary conditions
    nvelo(3) = 0;                       % boundary conditions
    nvelo(4) = 0;                       % boundary conditions
  
        nvelo_aver = nvelo;
 
% Compute A
    for n = 4:nodeCount-3
        if n==4
            A(n)=   (nvelo_aver(n+1) -  nvelo_aver(n))/2/nvolume(n);
        elseif n==nodeCount-3
            A(n)=   (nvelo_aver(n) -  nvelo_aver(n-1))/2/nvolume(n);
        else
            A(n)=   (nvelo_aver(n+1) -  nvelo_aver(n-1))/2/nvolume(n);
        end
    end
    
%% update particle stresses
    for e=1:elemCount
        
    esctr = elements(e,:);
    enode = nodes(esctr);
    mpts = mpoints{e};
    
    for p=1:length(mpts)                % loop over particles
    pid = mpts(p);
    
% compute shape functions N1, N2
    if choice ==1
    [N1,N2,dN1,dN2]=linearshape(xp(pid),enode(1),enode(2));
    N0=0;dN0=0;N3=0;dN3=0;
    elseif choice == 2
    [N0,dN0]=GIMPshape(xp(pid),enode(1)-deltal,deltal,Vp(pid));
    [N1,dN1]=GIMPshape(xp(pid),enode(1),deltal,Vp(pid));
    [N2,dN2]=GIMPshape(xp(pid),enode(2),deltal,Vp(pid));
    [N3,dN3]=GIMPshape(xp(pid),enode(2)+deltal,deltal,Vp(pid));   
    elseif choice==3
    [N1,N2,dN1,dN2,alpha]=DDMPshape(xp(pid),enode(1),enode(2));
    N0=0;dN0=0;N3=0;dN3=0;
    elseif choice == 4
    [N0,dN0]=Bsplineshape(xp(pid),enode(1)-deltal,deltal);
    [N1,dN1]=Bsplineshape(xp(pid),enode(1),deltal);
    [N2,dN2]=Bsplineshape(xp(pid),enode(2),deltal);
    [N3,dN3]=Bsplineshape(xp(pid),enode(2)+deltal,deltal); 
    end

    if choice ==3 
    Lp2(pid) = (N1 * A(esctr(1)) + N2 * A(esctr(2)) + N0 * A(esctr(1)-1) + N3 * A(esctr(2)+1));
    Lp1(pid) = dN1 *  nvelo(esctr(1)) + dN2 *  nvelo(esctr(2)) + dN0 *  nvelo(esctr(1)-1) + dN3 *  nvelo(esctr(2)+1);
    Lp(pid) = alpha * Lp1(pid) + (1 - alpha) * Lp2(pid);
    Fp(pid) = (1 + Lp(pid)* dtime) * Fp (pid);              % deformation tensor
    Vp(pid) = Fp (pid) * Vp0(pid);                          % update volume of particle
    dEps(pid) = 0;
    dEps1(pid) = dtime * Lp1(pid);                          % strain rate  
    dEps2(pid) = dtime * Lp2(pid);   
    ep1(pid) = ep1(pid) + dEps1(pid);
    ep2(pid) = ep2(pid) + dEps2(pid);
    epf1(p,1) = ep1(pid);
    else
    Lp(pid) = 1 * (dN1 * nvelo(esctr(1)) + dN2 * nvelo(esctr(2)) + dN0 * nvelo(esctr(1)-1) + dN3 * nvelo(esctr(2)+1));
    Fp(pid) = (1 + Lp(pid)* dtime) * Fp (pid);              % deformation tensor
    Vp(pid) = Fp (pid) * Vp0(pid);                          % update volume of particle
    dEps(pid) = dtime * Lp(pid);                            % strain rate 
    ep(pid) = ep(pid) + dEps(pid);
    epf(p,1) = ep(pid);
    end
    
    % Build matrix and run null space
    Np=Np+1;
    if cfilter==2 || cfilter==3
    dS(1,p)       = dN1;
    dS(2,p)       = dN2; 
    end
    end
   
    %% Null space filter algorithm
    if isempty(mpts)==0
        
        % House holder algorithm
        if cfilter==1
        if Np>1
        w = dN1/sqrt(Np*dN1.^2+Np/deltal/deltal+2*sqrt(Np)*dN1/deltal) * ones(Np,1);
        q1 = zeros(Np,1);
        w(1) = w(1)+sqrt(Np)/deltal/sqrt(Np*dN1.^2+Np/deltal/deltal+2*sqrt(Np)*dN1/deltal);
        q1(1)=q1(1)+1;
        for i=1:Np
            q1(i)=q1(i)-2*w(1)*w(i);
            if choice==3
            u = u+q1(i)*epf1(i);
            else
            u = u+q1(i)*epf(i);  
            end
        end
        if choice==3
        epf1=u*q1;
        else
        epf=u*q1;    
        end
        end
        u=0;
        Np=0;
        
        % local SVD algorithm
        elseif cfilter==2
        if choice ==3
            [epf1,~] = filter1(epf1,dS);
        else
            [epf,~] = filter1(epf,dS);
        end
        
        % local QR algorithm
        elseif cfilter==3
        if choice ==3
            [epf1] = filter2(epf1,dS',1); 
        else
            [epf] = filter2(epf,dS',1); 
        end
        end
    end

    for p=1:length (mpts)
        pid     = mpts(p);
        if choice ==3
        ep1(pid) = epf1(p,1);
        else
        ep(pid) = epf(p,1);
        end
    end
    end
        dS=[];
        epf=[];
        
    if choice ==3
    for e=3:elemCount-2
        
    esctr = elements(e,:);
    enode = nodes(esctr);
    mpts = mpoints{e};
    
    for p=1:length(mpts)                % loop over particles
    pid = mpts(p);
    
% compute shape functions N1, N2
    [~,~,~,~,alpha]=DDMPshape(xp(pid),enode(1),enode(2));
                ep(pid) = alpha * ep1(pid) + (1 - alpha) * ep2(pid);
    end
    end
    end
    
        if isnan(mean(ep))==1
         break
        end
    
    for pid=1:pCount
            sp(pid)         = E * ep(pid);               % stress
    end
    %% next step
    time = [time t];
    stress = [stress sp(1)];
    t=t+dtime;
    sum(nmomentum);
    check = [check sum(niforce)];
    
    % Store old nodal accerleration
    for n=4:nodeCount-3
    nacc_old(n) = nacc(n);
    end
end

x1=[0 0.25];y1=[0 0];x2=[0.25 0.25];y2=[0 1];x3=[0.25 0.75];y3=[1 1];x4=[0.75 0.75];y4=[1 0];x5=[0.75 1];y5=[0 0];
    figure
    plot(xp,sp,'.',x1,y1,'g',x2,y2,'g',x3,y3,'g',x4,y4,'g',x5,y5,'g');
    line(xp,sp);
    axis([0 1 -inf inf])
    ylabel('Stress'); % label for y axis
    xlabel('Length (s)'); % label for x axis
    if choice == 1
    legend('t=0.75*L/c(MPM)','analytical solution','Location','south');
    title('Stress plot of MPM solution in 1D');
    elseif choice == 2
    legend('t=0.75*L/c(GIMP)','analytical solution','Location','south');
    title('Stress plot of GIMP solution in 1D');
    elseif choice == 3
    title('Stress plot of DDMP solution in 1D');
    legend('t=0.75*L/c(DDMP)','analytical solution','Location','south');
    end
    toc
    