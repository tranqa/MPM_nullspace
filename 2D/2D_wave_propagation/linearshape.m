function [N,dN1,dN2]=linearshape(xp,xn,Lx,Ly)

    Nx = 1-abs(xp(1)-xn(1))/Lx;                       
    Ny = 1-abs(xp(2)-xn(2))/Ly;     
    
    N = Nx*Ny;
    
    dNx = -sign(xp(1)-xn(1))/Lx;
    dNy = -sign(xp(2)-xn(2))/Ly;
    
    dN1 = dNx*Ny;
    dN2 = Nx*dNy;
   
end