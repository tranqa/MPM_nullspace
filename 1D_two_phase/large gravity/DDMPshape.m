function [N1,N2,dN1,dN2,alpha]=DDMPshape(xp,xn1,xn2)

    Le = xn2 - xn1;
    N1 = (xn2-xp)/Le;                       
    N2 = (xp-xn1)/Le;
    dN1 = -1/Le; 
    dN2 =  1/Le;
   
    x=xp-xn1;
    
    alpha=  1 * (2*N1*2*N2).^1.5;
    end