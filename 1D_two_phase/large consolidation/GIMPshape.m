function [N,dN]=GIMPshape(xp,xn,dcell,dparticle)

%     if abs(xp - xn) < (dparticle/2)
%         N   = 1 - ((xp-xn).^2 +(dparticle/2).^2)/2/dcell/(dparticle/2);
%         dN  = -(xp-xn)/dcell/(dparticle/2);
%         
%     elseif (dparticle/2) <= abs(xp - xn) && abs(xp - xn)<= dcell - (dparticle/2)
%         N   = 1 - abs(xp-xn)/dcell;
%         dN  = -sign(xp-xn)/dcell;
%         
%     elseif dcell - (dparticle/2) <= abs(xp - xn) && abs(xp - xn)<= dcell + (dparticle/2)
%         N   = (dcell + (dparticle/2) - abs(xp - xn)).^2/4/dcell/(dparticle/2);
%         dN  = -(dcell + (dparticle/2) - abs(xp - xn))/2/dcell/(dparticle/2);
%         
%     else 
%         N   = 0;
%         dN  = 0;            


L=dcell;
lp=dparticle/2;
dx= xp - xn;

if dx > -L-lp && dx <= -L+lp
    N = (L+lp+dx).^2.0/(4.0*L*lp);
    dN = (L+lp+dx)/(2.0*L*lp);
    
elseif dx > -L+lp && dx <= -lp
    N = 1.0 + dx/L;
    dN = 1.0/L;
    
elseif dx > -lp && dx <= lp
    N = 1.0 - (dx.^2.0+lp.^2.0)/(2.0*L*lp);
    dN = -dx/L/lp;
    
elseif dx > lp && dx <= L-lp
    N = 1.0-(dx/L);
    dN = -1.0/L;
    
elseif dx > L-lp && dx <= L+lp
    N = (L+lp-dx).^2/(4.0*L*lp);
    dN = -(L+lp-dx)/(2.0*L*lp);
    
else
    N = 0.0;
    dN = 0.0;
end  
    end