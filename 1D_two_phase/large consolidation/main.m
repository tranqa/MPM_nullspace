clear all;
close all;
tic;

% Unit
% kN - seconds - m

% Traction force in Pa
ppp = 2000000;

%% time variable
choice                  = 5             ;
% choice=1 MPM 
% choice=2 u_GIMP 
% choice=3 cp_GIMP
% choice=4 DDMP
% choice=5 DDMP with nullspace filter

bound                   = 1              ;
% bound=1 fix boundary
% bound=2 absorbed boundary

TimeInte                = 1              ;
% TimeInte=1 linear time integration
% TImeInte=2 alpha generalized time integration
p_b = 0.5;
a_m = (2*p_b-1)/(1+p_b);
beta = (5-3*p_b)/(1+p_b).^2/(2-p_b);
gamma = 3/2-a_m;

delete_particle         = 1              ;
ftime                   = 0.5;

ft                      = ftime          ;                    % final time
t                       = 0              ;                    % time step
dt                      = 0.000001       ;                    % initial timpe
time = [];
drag = []; 
check = [];
Set_ana = [];

sVelocity1 = [];
wVelocity1 = [];
sVelocity2 = [];
wVelocity2 = [];
sVelocity1_ana1 = [];
wVelocity1_ana1 = [];
sVelocity2_ana1 = [];
wVelocity2_ana1 = [];

damps                    = 00.0        ;                   % Damping for solid
dampw                    = 00.0        ;                   % Damping for water

dampLocals               = 000         ;
dampLocalw               = 000         ;

%% Material Properties
E                       = 10e6       ;                      % Young modulus of solid
Kw                     = 2200e6          ; 
psp                     = 2143.0         ;                  % solid density
pwp                     = 1000.0         ;                  % liquid density
g                       = 0.0              ;
g_drag                  = 9.81;
k                       = 0.001         ;                   % permeability
n_o                     = 0.3            ;                  % porousity

%% Grid generation
if bound==2
L                       = 2.08           ;                   % total length
ncell                   = 104             ;                  % total number of cells
end

if bound==1
L                       = 1.08           ;                   % total length
ncell                   = 54             ;
end
sparticle_per_cell      = 2              ;                   % number SMP per cell
wparticle_per_cell      = 2              ;                   % number WMP per cell

nodes                   = linspace(0,L,ncell+1);            % list of node numbers (0-ncell)
cells                   = zeros(ncell,2);                   % begin and end node of each cell
for c=1:ncell
    cells(c,:)          = c:c+1;
end
nodeCount               = size(nodes,2);                    % number of nodes
dcell                   = L/ncell;                          % cell volume
dparticles               = dcell/sparticle_per_cell;        % SMP volume
dparticlew               = dcell/wparticle_per_cell;        % WMP volume

%% Nodal quantities
nmass_si                = zeros(nodeCount,1);               % nodal solid mass
nvolume_si              = zeros(nodeCount,1);               % nodal solid volume
nstress_si              = zeros(nodeCount,1);               % nodal solid stress
nmomentum_si            = zeros(nodeCount,1);               % nodal solid momentum
niforce_si              = zeros(nodeCount,1);               % nodal solid internal force
neforce_si              = zeros(nodeCount,1);               % nodal solid force
nforce_si              = zeros(nodeCount,1);                % nodal solid external force
nvelo_si                = zeros(nodeCount,1);               % nodal solid velocity
traction                = zeros(nodeCount,1);               % nodal traction
n_si                    = zeros(nodeCount,1);               % nodal porousity
k_si                    = zeros(nodeCount,1);               % nodal permeability
A_si                    = zeros(nodeCount,1); 

nacc_si                 = zeros(nodeCount,1);
nacc_old_si             = zeros(nodeCount,1);
nacc_middle_si          = zeros(nodeCount,1);

nmass_wi                = zeros(nodeCount,1);               % nodal liquid mass
nmomentum_wi            = zeros(nodeCount,1);               % nodal liquid momentum
nvolume_wi              = zeros(nodeCount,1);               % nodal liquid volume
npore_wi                = zeros(nodeCount,1);               % nodal liquid stress
npore_wi1               = zeros(nodeCount,1);               % nodal liquid stress
niforce_cell            = zeros(nodeCount,1);               % nodal liquid internal force fromc cell
niforce_wi              = zeros(nodeCount,1);               % nodal liquid internal force
niforce_wsi             = zeros(nodeCount,1);               % nodal liquid internal force
neforce_wi              = zeros(nodeCount,1);               % nodal liquid external force
nforce_wi               = zeros(nodeCount,1);               % nodal liquid force
nvelo_wi                = zeros(nodeCount,1);               % nodal liquid velocity
ndrag                   = zeros(nodeCount,1);               % nodal drag force
alpha                   = zeros(nodeCount,1);               % alpha to stablize porousity
alpha1                  = zeros(nodeCount,1);               % numerator of alpha to stablize porousity
alpha2                  = zeros(nodeCount,1);               % denominator of alpha to stablize porousity
dalpha                  = zeros(nodeCount,1);
A_wi                    = zeros(nodeCount,1);

nacc_wi                 = zeros(nodeCount,1);
nacc_old_wi             = zeros(nodeCount,1);
nacc_middle_wi          = zeros(nodeCount,1);

% Cell quantities
n_in_cell               = zeros(ncell,1);                   % number of particle in 1 cell
pore_cell               = zeros(ncell,1);                   % pore water pressure in 1 cell
alpha_cell              = zeros(ncell,1);

%% Solid particles
x_sp                    = zeros(sparticle_per_cell*(ncell-4),1); % solid particle position
spCount                 = length (x_sp);                    % number of solid particles

% generate the position along the grid
for sp=1:spCount
    x_sp(sp)=2*dcell + 0.5*(dparticles)+(sp-1)*(dparticles);
end

x_spo                   = x_sp;                             % initial position
V_sp                    = dparticles * ones(spCount,1);     % solid particle volume
V_spo                   = V_sp;                             % initial solid particle volume
F_sp                    = ones(spCount,1);                  % gradient deformation
m_sp                    = psp * (1-n_o) * V_sp;             % solid particle mass
s_sp                    = zeros(spCount,1);                 % solid particle effective stress
s_sp_ana                = zeros(spCount,1);                 % solid particle effective stress
v_ssp                   = zeros(spCount,1);                 % solid particle velocity
n_sp                    = n_o * ones(spCount,1);            % solid particle porousity
n_spo                   = n_o * ones(spCount,1);            % solid initial particle porousity
k_sp                    = k * ones(spCount,1);              % solid particle permeability
k_spo                   = k_sp;
b_sp                    = g * ones(spCount,1);              % solid particle body forces
L_sp                    = zeros(spCount,1);
L_sp1                   = zeros(spCount,1);
L_sp2                   = zeros(spCount,1);
dEps                    = zeros(spCount,1);
dN_si_matrix            = zeros(nodeCount,spCount);
spElems                 = zeros(spCount,1);


%% Liquid particles
x_wp                    = zeros(wparticle_per_cell*(ncell-4),1); % liquid particle position
wpCount                 = length (x_wp);                    % number of liquid particles
wpCounto = wpCount;

% generate the position along the grid
for wp=1:wpCount
    x_wp(wp)=2*dcell + 0.5*(dparticlew)+(wp-1)*(dparticlew);
end

x_wpo                   = x_wp;                             % initial position
V_wp                    = dparticlew * ones(wpCount,1);     % liquid particle volume
V_wpo                   = V_wp;                             % initial liquid particle volume
V_wpo_instri            = V_wpo * n_o;                      % initial intrinsic liquid particle volume
m_wp                    = pwp * n_o * V_wp;                 % liquid particle mass
pore_wp                 = -ppp * ones(wpCount,1);           % liquid particle effective stress
pore_wp_ana             = zeros(wpCount,1);                 % liquid particle effective stress
v_wp                    = zeros(wpCount,1);                 % liquid particle velocity
n_wp                    = zeros(wpCount,1);                 % liquid particle porousity
k_wp                    = zeros(wpCount,1);                 % liquid particle permeability
b_wp                    = g * ones(wpCount,1);              % liquid particle body forces
L_wp                    = zeros(wpCount,1);
L_swp                   = zeros(wpCount,1);
L_n                     = zeros(wpCount,1);
de_wwp                  = zeros(wpCount,1);
de_swp                  = zeros(wpCount,1);
de_wp                   = zeros(wpCount,1);
dN_wi_matrix            = zeros(nodeCount,wpCount);
wpElems                 = zeros(wpCount,1);

% Traction boundary
traction_sp             = zeros(spCount,1);
traction_wp             = zeros(wpCount,1);

%% start the algorithm
timestep = 2000;    % number of frame to save
r=timestep/20;      % number of frame per second video ~200s

writerObj3           = VideoWriter('pore_pressure.avi');
writerObj3.FrameRate = r;    % number of frame per second
open(writerObj3);
    for tt = 1:timestep
    ft              = ftime/timestep*tt;
              
 while t<ft      
     t;
traction_sp(1) = ppp;
  
%% Reset value of node
nmass_si(:)                = 0;
nmomentum_si(:)            = 0;  
niforce_si(:)              = 0;
neforce_si(:)              = 0;
nforce_si(:)               = 0;

nstress_si(:)              = 0;
nvolume_si(:)              = 0; 
nvelo_si(:)                = 0;
n_si(:)                    = 0;
k_si(:)                    = 0;
nacc_si(:)                 = 0;
nacc_middle_si(:)          = 0;

nmass_wi(:)                = 0;
nmomentum_wi(:)            = 0;  
niforce_wi(:)              = 0;
niforce_wsi(:)             = 0;
niforce_cell(:)            = 0;              
neforce_wi(:)              = 0;
nforce_wi(:)               = 0;

npore_wi(:)                = 0;
npore_wi1(:)               = 0;
nvolume_wi(:)              = 0;
nvelo_wi(:)                = 0;
nacc_wi(:)                 = 0;
nacc_middle_wi(:)          = 0;

dalpha(:)                  = 0;
ndrag(:)                   = 0;
n_wp(:)                    = 0;
k_wp(:)                    = 0;

alpha(:)                   = 0;
alpha1(:)                  = 0;
alpha2(:)                  = 0;
n_in_cell(:)               = 0;
pore_cell(:)               = 0;
alpha_cell(:)              = 0; 

dN_wi_matrix(:)              = 0;
dN_si_matrix(:)              = 0;

% Absorbed boundary
if bound==2
    traction_sp(spCount)=-2.5*sqrt(E/psp)*v_ssp(spCount)*psp - E/0.5*(x_sp(spCount)-x_spo(spCount));
    traction_wp(wpCount)=-2.5*sqrt(Kw/pwp)*v_wp(wpCount)*pwp - Kw/0.5*(x_wp(wpCount)-x_wpo(wpCount));
end

%% Store the particles into cell for solid phase
mspoints=cell(ncell,1);                                     % matrix to store particles for each cells

% define the position of particle in which cell
for sp=1:spCount
x = x_sp(sp);
c = (floor(x/dcell)+1);
spElems(sp) = c;                                            % Particle p stay in element c
end

% loop over cells and store particles in element "e"
for c=1:ncell
    id_sp = find(spElems==c);
    mspoints{c} = id_sp;                                    % mspoints {e} indicates particles in c
end

%% Store the particles into cell for liquid phase
mwpoints=cell(ncell,1);                                     % matrix to store particles for each cells

% define the position of particle in which cell
for wp=1:wpCount
x = x_wp(wp);
c = (floor(x/dcell)+1);
wpElems(wp) = c;                                            % Particle p stay in element c
end

% loop over cells and store particles in element "e"
for c=1:ncell
    id_wp = find(wpElems==c);
    mwpoints{c} = id_wp;                                    % mwpoints {e} indicates particles in c
end

%% Catching the boundary
snode_surface = spElems(1);
wnode_surface = wpElems(1);
for i=1:10
snode_surface = min(snode_surface,spElems(i+1));
wnode_surface = min(wnode_surface,wpElems(i+1));
end

%% update from particle to node for solid phase
for c =snode_surface:ncell-2                                % loop over computational cells
esctr = cells(c,:);                                         % nodes of element "c"
enode = nodes(esctr);                                       % nodal coordinates (begin and end)
mspts = mspoints{c};                                        % particles in element "c"
     
% Shape function
for sp=1:length(mspts)                                      % loop over particles inside 1 cell
    
    spid = mspts(sp);
    
    if choice ==1
     [N1,N2,dN1,dN2]=linearshape(x_sp(spid),enode(1),enode(2));
     N0=0;dN0=0;N3=0;dN3=0;
    elseif choice ==2
    [N0,dN0]=GIMPshape(x_sp(spid),enode(1)-dcell,dcell,dparticles);        
    [N1,dN1]=GIMPshape(x_sp(spid),enode(1),dcell,dparticles);
    [N2,dN2]=GIMPshape(x_sp(spid),enode(2),dcell,dparticles);
    [N3,dN3]=GIMPshape(x_sp(spid),enode(2)+dcell,dcell,dparticles);
    elseif choice ==3
    [N0,dN0]=GIMPshape(x_sp(spid),enode(1)-dcell,dcell,V_sp(spid));        
    [N1,dN1]=GIMPshape(x_sp(spid),enode(1),dcell,V_sp(spid));
    [N2,dN2]=GIMPshape(x_sp(spid),enode(2),dcell,V_sp(spid));
    [N3,dN3]=GIMPshape(x_sp(spid),enode(2)+dcell,dcell,V_sp(spid));
    elseif choice ==4 || choice ==5
    [N1,N2,dN1,dN2,falpha]=DDMPshape(x_sp(spid),enode(1),enode(2));
    N0=0;dN0=0;N3=0;dN3=0;
    end
    
% mass
nmass_si(esctr(1)-1)   = nmass_si(esctr(1)-1) + N0*m_sp(spid);
nmass_si(esctr(1))     = nmass_si(esctr(1)) + N1*m_sp(spid);
nmass_si(esctr(2))     = nmass_si(esctr(2)) + N2*m_sp(spid);
nmass_si(esctr(2)+1)   = nmass_si(esctr(2)+1) + N3*m_sp(spid);
 
% volume
nvolume_si(esctr(1)-1)= nvolume_si(esctr(1)-1) + N0*V_sp(spid);
nvolume_si(esctr(1))  = nvolume_si(esctr(1)) + N1*V_sp(spid);
nvolume_si(esctr(2))  = nvolume_si(esctr(2)) + N2*V_sp(spid);
nvolume_si(esctr(2)+1)= nvolume_si(esctr(2)+1) + N3*V_sp(spid);

% momentum
nmomentum_si(esctr(1)-1)= nmomentum_si(esctr(1)-1) + N0*m_sp(spid)*v_ssp(spid);
nmomentum_si(esctr(1))  = nmomentum_si(esctr(1)) + N1*m_sp(spid)*v_ssp(spid);
nmomentum_si(esctr(2))  = nmomentum_si(esctr(2)) + N2*m_sp(spid)*v_ssp(spid);
nmomentum_si(esctr(2)+1)= nmomentum_si(esctr(2)+1) + N3*m_sp(spid)*v_ssp(spid);

% internal force
if choice ==4 || choice ==5
niforce_si(esctr(1)-1)  = niforce_si(esctr(1)-1) - V_sp(spid)*s_sp(spid)*dN0*falpha;
niforce_si(esctr(1))    = niforce_si(esctr(1)) - V_sp(spid)*s_sp(spid)*dN1*falpha;
niforce_si(esctr(2))    = niforce_si(esctr(2)) - V_sp(spid)*s_sp(spid)*dN2*falpha;
niforce_si(esctr(2)+1)  = niforce_si(esctr(2)+1) - V_sp(spid)*s_sp(spid)*dN3*falpha;

else
niforce_si(esctr(1)-1)  = niforce_si(esctr(1)-1) - V_sp(spid)*s_sp(spid)*dN0;
niforce_si(esctr(1))    = niforce_si(esctr(1)) - V_sp(spid)*s_sp(spid)*dN1;
niforce_si(esctr(2))    = niforce_si(esctr(2)) - V_sp(spid)*s_sp(spid)*dN2;
niforce_si(esctr(2)+1)  = niforce_si(esctr(2)+1) - V_sp(spid)*s_sp(spid)*dN3;
end

% external force
neforce_si(esctr(1)-1)  = neforce_si(esctr(1)-1) + b_sp(spid)*m_sp(spid)*N0 + V_sp(spid)*traction_sp(spid)/dparticles*N0;
neforce_si(esctr(1))    = neforce_si(esctr(1)) + b_sp(spid)*m_sp(spid)*N1 + V_sp(spid)*traction_sp(spid)/dparticles*N1;
neforce_si(esctr(2))    = neforce_si(esctr(2)) + b_sp(spid)*m_sp(spid)*N2 + V_sp(spid)*traction_sp(spid)/dparticles*N2;
neforce_si(esctr(2)+1)  = neforce_si(esctr(2)+1) + b_sp(spid)*m_sp(spid)*N3 + V_sp(spid)*traction_sp(spid)/dparticles*N3;

% porousity
n_si(esctr(1)-1)    = n_si(esctr(1)-1) + n_sp(spid)*m_sp(spid)*N0;
n_si(esctr(1))      = n_si(esctr(1)) + n_sp(spid)*m_sp(spid)*N1;
n_si(esctr(2))      = n_si(esctr(2)) + n_sp(spid)*m_sp(spid)*N2;
n_si(esctr(2)+1)    = n_si(esctr(2)+1) + n_sp(spid)*m_sp(spid)*N3;

% permeability
k_si(esctr(1)-1)    = k_si(esctr(1)-1) + k_sp(spid)*m_sp(spid)*N0;
k_si(esctr(1))      = k_si(esctr(1)) + k_sp(spid)*m_sp(spid)*N1;

k_si(esctr(2))      = k_si(esctr(2)) + k_sp(spid)*m_sp(spid)*N2;
k_si(esctr(2)+1)    = k_si(esctr(2)+1) + k_sp(spid)*m_sp(spid)*N3;
end
end

for n = snode_surface:nodeCount-2
    if nmass_si(n)==0
        nmass_si(n-1)=(nmass_si(n-1)+nmass_si(n+1))/3;
        nmass_si(n)=(nmass_si(n-1)+nmass_si(n+1))/3;
        nmass_si(n+1)=(nmass_si(n-1)+nmass_si(n+1))/3;
    end
    
    
    if nvolume_si(n)==0
        nvolume_si(n-1)=(nvolume_si(n-1)+nvolume_si(n+1))/3;
        nvolume_si(n)=(nvolume_si(n-1)+nvolume_si(n+1))/3;
        nvolume_si(n+1)=(nvolume_si(n-1)+nvolume_si(n+1))/3;
    end
    end
    
%% Update permeability and porousity and volume for liquid phase
for n=snode_surface:nodeCount-2   
    n_si(n) = n_si(n)/nmass_si(n);
    k_si(n) = k_si(n)/nmass_si(n);
end

for c = wnode_surface:ncell-2
        esctr = cells(c,:);          
        enode = nodes(esctr);                 
        mwpts = mwpoints{c};              

% loop over particles
    for wp=1:length (mwpts)
        wpid = mwpts(wp);                  % loop each particle (spid) inside an element
        
% compute shape functions N1, N2
    if choice ==1
    [N1,N2,dN1,dN2]=linearshape(x_wp(wpid),enode(1),enode(2));
    N0=0;dN0=0;N3=0;dN3=0;
    elseif choice ==2
    [N0,dN0]=GIMPshape(x_wp(wpid),enode(1)-dcell,dcell,dparticlew);
    [N1,dN1]=GIMPshape(x_wp(wpid),enode(1),dcell,dparticlew);
    [N2,dN2]=GIMPshape(x_wp(wpid),enode(2),dcell,dparticlew);
    [N3,dN3]=GIMPshape(x_wp(wpid),enode(2)+dcell,dcell,dparticlew);
    elseif choice ==3
    [N0,dN0]=GIMPshape(x_wp(wpid),enode(1)-dcell,dcell,V_wp(wpid));
    [N1,dN1]=GIMPshape(x_wp(wpid),enode(1),dcell,V_wp(wpid));
    [N2,dN2]=GIMPshape(x_wp(wpid),enode(2),dcell,V_wp(wpid));
    [N3,dN3]=GIMPshape(x_wp(wpid),enode(2)+dcell,dcell,V_wp(wpid));
    elseif choice ==4 || choice ==5
    [N1,N2,dN1,dN2,falpha]=DDMPshape(x_wp(wpid),enode(1),enode(2));
    N0=0;dN0=0;N3=0;dN3=0;
    end
    
% Update permeability and porousity
    n_wp(wpid) = n_wp(wpid) + n_si(esctr(1))*N1 + n_si(esctr(2))*N2 + n_si(esctr(1)-1)*N0 + n_si(esctr(2)+1)*N3;
    k_wp(wpid) = k_wp(wpid) + k_si(esctr(1))*N1 + k_si(esctr(2))*N2 + k_si(esctr(1)-1)*N0 + k_si(esctr(2)+1)*N3;
    end
end

for wp=1:wpCount
    if k_wp(wp)==0
        k_wp(wp)=k_wp(wp-1);
        n_wp(wp)=n_wp(wp-1);
    end
end

%% Nodal Solid Effective Stress
if choice ==4 || choice ==5
    for c =snode_surface:ncell-2                                % loop over computational cells
    esctr = cells(c,:);                                         % nodes of element "c"
    enode = nodes(esctr);                                       % nodal coordinates (begin and end)
    mspts = mspoints{c};                                        % particles in element "c"
     
    % Shape function

    for sp=1:length(mspts)                                      % loop over particles inside 1 cell
    
    spid = mspts(sp);
    
    [N1,N2,dN1,dN2,falpha]=DDMPshape(x_sp(spid),enode(1),enode(2));
    N0=0;dN0=0;N3=0;dN3=0;
    
    nstress_si(esctr(1)-1) = nstress_si(esctr(1)-1) + (1-falpha) * V_sp(spid)*s_sp(spid)*N0;
    nstress_si(esctr(1))   = nstress_si(esctr(1)) + (1-falpha) * V_sp(spid)*s_sp(spid)*N1;
    nstress_si(esctr(2))   = nstress_si(esctr(2)) + (1-falpha) * V_sp(spid)*s_sp(spid)*N2;
    nstress_si(esctr(2)+1) = nstress_si(esctr(2)+1) + (1-falpha) * V_sp(spid)*s_sp(spid)*N3;
    end
    end

for n=snode_surface:nodeCount-2
    nstress_si(n) = nstress_si(n)/nvolume_si(n);
end

% Internal Solid force

for n=snode_surface:nodeCount-2
    if n==snode_surface
        niforce_si(n) = niforce_si(n) - (-nstress_si(n)-nstress_si(n+1))/2;
    elseif n==nodeCount-1
        niforce_si(n) = niforce_si(n) - (nstress_si(n-1)+nstress_si(n))/2;
    else
        niforce_si(n) = niforce_si(n) - (nstress_si(n-1)-nstress_si(n+1))/2;
    end
end
end

%% Phreatic boundary
if delete_particle==1
if wnode_surface < snode_surface
  for c = 1:wnode_surface;
    esctr = cells(c,:);
    enode = nodes(esctr);
    mwpts = mwpoints{c};
    
    for wp=1:length(mwpts)                % loop over particles
    wpid = mwpts(wp);
%     n_wp(wpid) = n_o;
    x_wp(mwpts)                    = [];       
    x_wpo(mwpts)                   = [];                        
    V_wp(mwpts)                    = [];                 
    V_wpo(mwpts)                   = [];                                         
    V_wpo_instri(mwpts)            = [];                                
    m_wp(mwpts)                    = [];                     
    b_wp(mwpts)                    = [];                  
    pore_wp(mwpts)                 = [];                 
    pore_wp_ana(mwpts)             = [];                            
    v_wp(mwpts)                    = [];                          
    n_wp(mwpts)                    = [];                         
    k_wp(mwpts)                    = [];                      
    L_swp(mwpts)                   = []; 
    L_wp(mwpts)                    = []; 
    dN_wi_matrix(:,mwpts)          = []; 
    de_wwp(mwpts)                  = []; 
    de_swp(mwpts)                  = []; 
    de_wp (mwpts)                  = []; 
    wpElems(mwpts)                 = []; 
    wpCount                        = length(x_wp);
    
    
    end    
  end
end

%% Store the particles into cell for liquid phase
mwpoints=cell(ncell,1);                                     % matrix to store particles for each cells

% define the position of particle in which cell
for wp=1:wpCount
x = x_wp(wp);
c = (floor(x/dcell)+1);
wpElems(wp) = c;                                            % Particle p stay in element c
end

% loop over cells and store particles in element "e"
for c=1:ncell
    id_wp = find(wpElems==c);
    mwpoints{c} = id_wp;                                    % mwpoints {e} indicates particles in c
end
end

%% Update volume for liquid phase
for wp=1:wpCount
    V_wp(wp) = (n_o * V_wpo(wp))/n_wp(wp);
end


%% Catching the boundary
wnode_surface = wpElems(1);
for i=1:10
wnode_surface = min(wnode_surface,wpElems(i+1));
end

%% update from particle to node for liquid phase
for c =3:ncell-2                                            % loop over computational cells
esctr = cells(c,:);                                         % nodes of element "c"
enode = nodes(esctr);                                       % nodal coordinates (begin and end)
mwpts = mwpoints{c};                                        % particles in element "c"
     
% Shape function
for wp=1:length(mwpts)                                      % loop over particles inside 1 cell
    
    wpid = mwpts(wp);
    
    if choice ==1
    [N1,N2,dN1,dN2]=linearshape(x_wp(wpid),enode(1),enode(2));
    N0=0;dN0=0;N3=0;dN3=0;
    elseif choice ==2
    [N0,dN0]=GIMPshape(x_wp(wpid),enode(1)-dcell,dcell,dparticlew);
    [N1,dN1]=GIMPshape(x_wp(wpid),enode(1),dcell,dparticlew);
    [N2,dN2]=GIMPshape(x_wp(wpid),enode(2),dcell,dparticlew);
    [N3,dN3]=GIMPshape(x_wp(wpid),enode(2)+dcell,dcell,dparticlew);
    [~,dN0_c]=GIMPshape(enode(1)+(enode(2)-enode(1))/2,enode(1)-dcell,dcell,dparticlew);
    [~,dN1_c]=GIMPshape(enode(1)+(enode(2)-enode(1))/2,enode(1),dcell,dparticlew);
    [~,dN2_c]=GIMPshape(enode(1)+(enode(2)-enode(1))/2,enode(2),dcell,dparticlew);
    [~,dN3_c]=GIMPshape(enode(1)+(enode(2)-enode(1))/2,enode(2)+dcell,dcell,dparticlew);
    elseif choice ==3
    [N0,dN0]=GIMPshape(x_wp(wpid),enode(1)-dcell,dcell,V_wp(wpid));
    [N1,dN1]=GIMPshape(x_wp(wpid),enode(1),dcell,V_wp(wpid));
    [N2,dN2]=GIMPshape(x_wp(wpid),enode(2),dcell,V_wp(wpid));
    [N3,dN3]=GIMPshape(x_wp(wpid),enode(2)+dcell,dcell,V_wp(wpid));
    [~,dN0_c]=GIMPshape(enode(1)+(enode(2)-enode(1))/2,enode(1)-dcell,dcell,V_wp(wpid));
    [~,dN1_c]=GIMPshape(enode(1)+(enode(2)-enode(1))/2,enode(1),dcell,V_wp(wpid));
    [~,dN2_c]=GIMPshape(enode(1)+(enode(2)-enode(1))/2,enode(2),dcell,V_wp(wpid));
    [~,dN3_c]=GIMPshape(enode(1)+(enode(2)-enode(1))/2,enode(2)+dcell,dcell,V_wp(wpid));
    elseif choice ==4 || choice ==5
    [N1,N2,dN1,dN2,falpha]=DDMPshape(x_wp(wpid),enode(1),enode(2));
    N0=0;dN0=0;N3=0;dN3=0;
    end
    
% mass
nmass_wi(esctr(1)-1)        = nmass_wi(esctr(1)-1) + N0*m_wp(wpid);
nmass_wi(esctr(1))          = nmass_wi(esctr(1)) + N1*m_wp(wpid);
nmass_wi(esctr(2))          = nmass_wi(esctr(2)) + N2*m_wp(wpid);
nmass_wi(esctr(2)+1)        = nmass_wi(esctr(2)+1) + N3*m_wp(wpid);

% volume
nvolume_wi(esctr(1)-1)        = nvolume_wi(esctr(1)-1) + N0*V_wp(wpid);
nvolume_wi(esctr(1))          = nvolume_wi(esctr(1)) + N1*V_wp(wpid);
nvolume_wi(esctr(2))          = nvolume_wi(esctr(2)) + N2*V_wp(wpid);
nvolume_wi(esctr(2)+1)        = nvolume_wi(esctr(2)+1) + N3*V_wp(wpid);

% momentum
nmomentum_wi(esctr(1)-1)    = nmomentum_wi(esctr(1)-1) + N0*m_wp(wpid)*v_wp(wpid);
nmomentum_wi(esctr(1))      = nmomentum_wi(esctr(1)) + N1*m_wp(wpid)*v_wp(wpid);
nmomentum_wi(esctr(2))      = nmomentum_wi(esctr(2)) + N2*m_wp(wpid)*v_wp(wpid);
nmomentum_wi(esctr(2)+1)    = nmomentum_wi(esctr(2)+1) + N3*m_wp(wpid)*v_wp(wpid);

%external force
neforce_wi(esctr(1)-1)      = neforce_wi(esctr(1)-1) + b_wp(wpid)*m_wp(wpid)*N0 + V_wp(wpid)*traction_wp(wpid)/dparticlew*N0;
neforce_wi(esctr(1))        = neforce_wi(esctr(1)) + b_wp(wpid)*m_wp(wpid)*N1 + V_wp(wpid)*traction_wp(wpid)/dparticlew*N0;
neforce_wi(esctr(2))        = neforce_wi(esctr(2)) + b_wp(wpid)*m_wp(wpid)*N2 + V_wp(wpid)*traction_wp(wpid)/dparticlew*N0;
neforce_wi(esctr(2)+1)      = neforce_wi(esctr(2)+1) + b_wp(wpid)*m_wp(wpid)*N3 + V_wp(wpid)*traction_wp(wpid)/dparticlew*N0;

%drag force
ndrag(esctr(1)-1)           = ndrag(esctr(1)-1) + m_wp(wpid)*g_drag*N0/k_wp(wpid);
ndrag(esctr(1))             = ndrag(esctr(1)) + m_wp(wpid)*g_drag*N1/k_wp(wpid);
ndrag(esctr(2))             = ndrag(esctr(2)) + m_wp(wpid)*g_drag*N2/k_wp(wpid);
ndrag(esctr(2)+1)           = ndrag(esctr(2)+1) + m_wp(wpid)*g_drag*N3/k_wp(wpid);

% alpha 1
alpha1(esctr(1)-1)         = alpha1(esctr(1)-1) + V_wp(wpid) * n_wp(wpid) * N0;
alpha1(esctr(1))           = alpha1(esctr(1)) + V_wp(wpid) * n_wp(wpid) * N1;
alpha1(esctr(2))           = alpha1(esctr(2)) + V_wp(wpid) * n_wp(wpid) * N2;
alpha1(esctr(2)+1)         = alpha1(esctr(2)+1) + V_wp(wpid) * n_wp(wpid) * N3;
end
end

%% Calculate the pore pressure in cell
for c =3:ncell-2                                            % loop over computational cells
esctr = cells(c,:);                                         % nodes of element "c"
enode = nodes(esctr);                                       % nodal coordinates (begin and end)
mwpts = mwpoints{c};                                        % particles in element "c"
     
% Shape function
for wp=1:length(mwpts)                                      % loop over particles inside 1 cell
    
    wpid = mwpts(wp);
    
    % pore pressure at cell
    pore_cell(c)               = pore_cell(c) + pore_wp(wpid);
    n_in_cell(c)               = n_in_cell(c) + 1;
    end
    end

%% Calculate internal force

    % pore water pressure at cells
    for c=wnode_surface:ncell-2
        if n_in_cell(c)==0
            break
        else
        pore_cell(c)           = pore_cell(c)/n_in_cell(c);
        end
    end
    
    for c =wnode_surface:ncell-2                                % loop over computational cells
    esctr = cells(c,:);                                         % nodes of element "c"
    enode = nodes(esctr);                                       % nodal coordinates (begin and end)
     
    % Shape function
     if choice ==1
    [N1_c,N2_c,dN1_c,dN2_c]=linearshape(enode(1)+(enode(2)-enode(1))/2,enode(1),enode(2));
    N0_c=0;dN0_c=0;N3_c=0;dN3_c=0;
    elseif choice ==2 || choice ==3
    [N0_c,dN0_c]=GIMPshape(enode(1)+(enode(2)-enode(1))/2,enode(1)-dcell,dcell,0);
    [N1_c,dN1_c]=GIMPshape(enode(1)+(enode(2)-enode(1))/2,enode(1),dcell,0);
    [N2_c,dN2_c]=GIMPshape(enode(1)+(enode(2)-enode(1))/2,enode(2),dcell,0);
    [N3_c,dN3_c]=GIMPshape(enode(1)+(enode(2)-enode(1))/2,enode(2)+dcell,dcell,0);
    elseif choice ==4 || choice ==5
    [N1_c,N2_c,dN1_c,dN2_c,falpha]=DDMPshape(enode(1)+(enode(2)-enode(1))/2,enode(1),enode(2));
    N0_c=0;dN0_c=0;N3_c=0;dN3_c=0;
     end
     
    % internal force in cells    
    if choice ==4 || choice ==5
    niforce_cell(esctr(1)-1)      = niforce_cell(esctr(1)-1) - dcell*pore_cell(c)*dN0_c*falpha;
    niforce_cell(esctr(1))        = niforce_cell(esctr(1)) - dcell*pore_cell(c)*dN1_c*falpha;
    niforce_cell(esctr(2))        = niforce_cell(esctr(2)) - dcell*pore_cell(c)*dN2_c*falpha;
    niforce_cell(esctr(2)+1)      = niforce_cell(esctr(2)+1) - dcell*pore_cell(c)*dN3_c*falpha;  
    else
    niforce_cell(esctr(1)-1)      = niforce_cell(esctr(1)-1) - dcell*pore_cell(c)*dN0_c;
    niforce_cell(esctr(1))        = niforce_cell(esctr(1)) - dcell*pore_cell(c)*dN1_c;
    niforce_cell(esctr(2))        = niforce_cell(esctr(2)) - dcell*pore_cell(c)*dN2_c;
    niforce_cell(esctr(2)+1)      = niforce_cell(esctr(2)+1) - dcell*pore_cell(c)*dN3_c;
    end
    
    % alpha 1
    alpha2(esctr(1)-1)         = alpha2(esctr(1)-1) + dcell * N0_c;
    alpha2(esctr(1))           = alpha2(esctr(1)) + dcell * N1_c;
    alpha2(esctr(2))           = alpha2(esctr(2)) + dcell * N2_c;
    alpha2(esctr(2)+1)         = alpha2(esctr(2)+1) + dcell * N3_c;
    end

    
    for n=wnode_surface:nodeCount-2
        alpha(n)               = alpha1(n)/alpha2(n);
    end
    
    %% Nodal Pore pressure
if choice ==4 || choice ==5
   for c =wnode_surface:ncell-2                                 % loop over computational cells
    esctr = cells(c,:);                                         % nodes of element "c"
    enode = nodes(esctr);                                       % nodal coordinates (begin and end)
     
    % Shape function     
    [N1_c,N2_c,dN1_c,dN2_c,falpha]=DDMPshape(enode(1)+(enode(2)-enode(1))/2,enode(1),enode(2));
    N0_c=0;dN0_c=0;N3_c=0;dN3_c=0;
    
    npore_wi(esctr(1)-1) = npore_wi(esctr(1)-1) + (1-falpha) * dcell*pore_cell(c)*N0;
    npore_wi(esctr(1))   = npore_wi(esctr(1)) + (1-falpha) * dcell*pore_cell(c)*N1;
    npore_wi(esctr(2))   = npore_wi(esctr(2)) + (1-falpha) * dcell*pore_cell(c)*N2;
    npore_wi(esctr(2)+1) = npore_wi(esctr(2)+1) + (1-falpha) * dcell*pore_cell(c)*N3;
    
    npore_wi1(esctr(1)-1) = npore_wi1(esctr(1)-1) + dcell*pore_cell(c)*N0;
    npore_wi1(esctr(1))   = npore_wi1(esctr(1)) + dcell*pore_cell(c)*N1;
    npore_wi1(esctr(2))   = npore_wi1(esctr(2)) + dcell*pore_cell(c)*N2;
    npore_wi1(esctr(2)+1) = npore_wi1(esctr(2)+1) + dcell*pore_cell(c)*N3;
    
    end
    
for n=wnode_surface:nodeCount-2
    npore_wi(n) = npore_wi(n)/nvolume_wi(n);
    npore_wi1(n) = npore_wi1(n)/nvolume_wi(n);
end

% Internal Solid force

for n=wnode_surface:nodeCount-2
    if n==wnode_surface
        niforce_cell(n) = niforce_cell(n) - (-npore_wi(n)-npore_wi(n+1))/2;
    elseif n==nodeCount-2
        niforce_cell(n) = niforce_cell(n) - (npore_wi(n-1)+npore_wi(n))/2;
    else
        niforce_cell(n) = niforce_cell(n) - (npore_wi(n-1)-npore_wi(n+1))/2;
    end
end
end
        
%% Drag force
for n=max(wnode_surface,snode_surface):nodeCount-2
    if nmass_si(n) ==0
        break
    else
    ndrag(n) = ndrag(n) * (nmomentum_wi(n)/nmass_wi(n) - nmomentum_si(n)/nmass_si(n)) + npore_wi1(n)*dalpha(n);
    end
end  

%% calculate alpha and internal force
    for n=wnode_surface:nodeCount-2
        niforce_wi(n)          = alpha(n) * niforce_cell(n);
    end

%% update nodal momenta
for n=1:nodeCount
% Liquid forces
nforce_wi(n) =  niforce_wi(n) + neforce_wi(n) - alpha(n)*ndrag(n);
nforce_wi(n) = nforce_wi(n) - dampLocalw*nmomentum_wi(n);

% Solid forces
nforce_si(n) = niforce_si(n) + niforce_cell(n) +  neforce_si(n) + neforce_wi(n) + traction(n) - nforce_wi(n);
nforce_si(n) = nforce_si(n) - dampLocals*nmomentum_si(n);
% Liquid momentum
nmomentum_wi(n) = nmomentum_wi(n) + nforce_wi(n)*dt;
% Solid momentum
nmomentum_si(n) = nmomentum_si(n) + nforce_si(n)*dt;
end

% Boundary conditions
if bound==1
nmomentum_si(nodeCount) = 0;
nforce_si(nodeCount) = 0;
nmomentum_wi(nodeCount) = 0; 
nforce_wi(nodeCount) = 0;

nmomentum_si(nodeCount-1) = 0;
nforce_si(nodeCount-1) = 0;
nmomentum_wi(nodeCount-1) = 0; 
nforce_wi(nodeCount-1) = 0;

nmomentum_si(nodeCount-2) = 0;
nforce_si(nodeCount-2) = 0;
nmomentum_wi(nodeCount-2) = 0; 
nforce_wi(nodeCount-2) = 0;
end

%% Calculate accerleration for time integration
if TimeInte==2
    
for n=wnode_surface:nodeCount-2
    nacc_middle_wi(n) = nforce_wi(n)/nmass_wi(n);
    nacc_wi(n)=(nacc_middle_wi(n) - a_m*nacc_old_wi(n))/(1-a_m);
end

for n=snode_surface:nodeCount-2
    nacc_middle_si(n) = nforce_si(n)/nmass_si(n);
    nacc_si(n)=(nacc_middle_si(n) - a_m*nacc_old_si(n))/(1-a_m);
end

end
%% update solid particle velocity and position; recompute nodal velocities
    for c = snode_surface:ncell-2
        esctr = cells(c,:);          
        enode = nodes(esctr);                 
        mspts = mspoints{c};              

% loop over particles
    for sp=1:length (mspts)
        spid = mspts(sp);                  % loop each particle (spid) inside an element
        
% compute shape functions N1, N2
    if choice ==1
    [N1,N2,dN1,dN2]=linearshape(x_sp(spid),enode(1),enode(2));
    N0=0;dN0=0;N3=0;dN3=0;
    elseif choice ==2
    [N0,dN0]=GIMPshape(x_sp(spid),enode(1)-dcell,dcell,dparticles);        
    [N1,dN1]=GIMPshape(x_sp(spid),enode(1),dcell,dparticles);
    [N2,dN2]=GIMPshape(x_sp(spid),enode(2),dcell,dparticles);
    [N3,dN3]=GIMPshape(x_sp(spid),enode(2)+dcell,dcell,dparticles);
    elseif choice ==3
    [N0,dN0]=GIMPshape(x_sp(spid),enode(1)-dcell,dcell,V_sp(spid));        
    [N1,dN1]=GIMPshape(x_sp(spid),enode(1),dcell,V_sp(spid));
    [N2,dN2]=GIMPshape(x_sp(spid),enode(2),dcell,V_sp(spid));
    [N3,dN3]=GIMPshape(x_sp(spid),enode(2)+dcell,dcell,V_sp(spid));
    elseif choice ==4 || choice ==5
    [N1,N2,dN1,dN2,falpha]=DDMPshape(x_sp(spid),enode(1),enode(2));
    N0=0;dN0=0;N3=0;dN3=0;
    end
    
%Update particle velocity and position
if TimeInte==1
    
    if esctr(1)==snode_surface
    v_ssp(spid) = v_ssp(spid) + dt * N1 * (nforce_si(esctr(1))/nmass_si(esctr(1)) - damps * nmomentum_si(esctr(1))/nmass_si(esctr(1))); 
    v_ssp(spid) = v_ssp(spid) + dt * N2 * (nforce_si(esctr(2))/nmass_si(esctr(2)) - damps * nmomentum_si(esctr(2))/nmass_si(esctr(2)));
    v_ssp(spid) = v_ssp(spid) + dt * N3 * (nforce_si(esctr(2)+1)/nmass_si(esctr(2)+1) - damps * nmomentum_si(esctr(2)+1)/nmass_si(esctr(2)+1));

    x_sp(spid) = x_sp(spid) + dt * N1 * nmomentum_si(esctr(1))/nmass_si(esctr(1));
    x_sp(spid) = x_sp(spid) + dt * N2 * nmomentum_si(esctr(2))/nmass_si(esctr(2));
    x_sp(spid) = x_sp(spid) + dt * N3 * nmomentum_si(esctr(2)+1)/nmass_si(esctr(2)+1);

    elseif esctr(2)==nodeCount-2
    v_ssp(spid) = v_ssp(spid) + dt * N1 * (nforce_si(esctr(1))/nmass_si(esctr(1)) - damps * nmomentum_si(esctr(1))/nmass_si(esctr(1))); 
    v_ssp(spid) = v_ssp(spid) + dt * N2 * (nforce_si(esctr(2))/nmass_si(esctr(2)) - damps * nmomentum_si(esctr(2))/nmass_si(esctr(2)));
    v_ssp(spid) = v_ssp(spid) + dt * N0 * (nforce_si(esctr(1)-1)/nmass_si(esctr(1)-1) - damps * nmomentum_si(esctr(1)-1)/nmass_si(esctr(1)-1));

    x_sp(spid) = x_sp(spid) + dt * N1 * nmomentum_si(esctr(1))/nmass_si(esctr(1));
    x_sp(spid) = x_sp(spid) + dt * N2 * nmomentum_si(esctr(2))/nmass_si(esctr(2));
    x_sp(spid) = x_sp(spid) + dt * N0 * nmomentum_si(esctr(1)-1)/nmass_si(esctr(1)-1);
    
    else
    v_ssp(spid) = v_ssp(spid) + dt * N1 * (nforce_si(esctr(1))/nmass_si(esctr(1)) - damps * nmomentum_si(esctr(1))/nmass_si(esctr(1))); 
    v_ssp(spid) = v_ssp(spid) + dt * N2 * (nforce_si(esctr(2))/nmass_si(esctr(2)) - damps * nmomentum_si(esctr(2))/nmass_si(esctr(2)));
    v_ssp(spid) = v_ssp(spid) + dt * N0 * (nforce_si(esctr(1)-1)/nmass_si(esctr(1)-1) - damps * nmomentum_si(esctr(1)-1)/nmass_si(esctr(1)-1));
    v_ssp(spid) = v_ssp(spid) + dt * N3 * (nforce_si(esctr(2)+1)/nmass_si(esctr(2)+1) - damps * nmomentum_si(esctr(2)+1)/nmass_si(esctr(2)+1));

    x_sp(spid) = x_sp(spid) + dt * N1 * nmomentum_si(esctr(1))/nmass_si(esctr(1));
    x_sp(spid) = x_sp(spid) + dt * N2 * nmomentum_si(esctr(2))/nmass_si(esctr(2));
    x_sp(spid) = x_sp(spid) + dt * N0 * nmomentum_si(esctr(1)-1)/nmass_si(esctr(1)-1);
    x_sp(spid) = x_sp(spid) + dt * N3 * nmomentum_si(esctr(2)+1)/nmass_si(esctr(2)+1);
    end
    
elseif TimeInte==2
     if esctr(1)==snode_surface
    v_ssp(spid) = v_ssp(spid) + dt * N1 * ((1-gamma)*nacc_old_si(esctr(1))+gamma*nacc_si(esctr(1)) - damps* nmomentum_si(esctr(1))/nmass_si(esctr(1))); 
    v_ssp(spid) = v_ssp(spid) + dt * N2 * ((1-gamma)*nacc_old_si(esctr(2))+gamma*nacc_si(esctr(2)) - damps* nmomentum_si(esctr(2))/nmass_si(esctr(2)));
    v_ssp(spid) = v_ssp(spid) + dt * N3 * ((1-gamma)*nacc_old_si(esctr(2)+1)+gamma*nacc_si(esctr(2)+1) - damps * nmomentum_si(esctr(2)+1)/nmass_si(esctr(2)+1));

    x_sp(spid) = x_sp(spid) + dt * N1 * (nmomentum_si(esctr(1))/nmass_si(esctr(1)) + dt*((0.5-beta)*nacc_old_si(esctr(1))+beta*nacc_si(esctr(1))));
    x_sp(spid) = x_sp(spid) + dt * N2 * (nmomentum_si(esctr(2))/nmass_si(esctr(2)) + dt*((0.5-beta)*nacc_old_si(esctr(2))+beta*nacc_si(esctr(2))));
    x_sp(spid) = x_sp(spid) + dt * N3 * (nmomentum_si(esctr(2)+1)/nmass_si(esctr(2)+1) + dt*((0.5-beta)*nacc_old_si(esctr(2)+1)+beta*nacc_si(esctr(2)+1)));

    elseif esctr(2)==nodeCount-2
    v_ssp(spid) = v_ssp(spid) + dt * N1 * ((1-gamma)*nacc_old_si(esctr(1))+gamma*nacc_si(esctr(1)) - damps * nmomentum_si(esctr(1))/nmass_si(esctr(1))); 
    v_ssp(spid) = v_ssp(spid) + dt * N2 * ((1-gamma)*nacc_old_si(esctr(2))+gamma*nacc_si(esctr(2)) - damps * nmomentum_si(esctr(2))/nmass_si(esctr(2)));
    v_ssp(spid) = v_ssp(spid) + dt * N0 * ((1-gamma)*nacc_old_si(esctr(1)-1)+gamma*nacc_si(esctr(1)-1) - damps * nmomentum_si(esctr(1)-1)/nmass_si(esctr(1)-1));

    x_sp(spid) = x_sp(spid) + dt * N1 * (nmomentum_si(esctr(1))/nmass_si(esctr(1)) + dt*((0.5-beta)*nacc_old_si(esctr(1))+beta*nacc_si(esctr(1))));
    x_sp(spid) = x_sp(spid) + dt * N2 * (nmomentum_si(esctr(2))/nmass_si(esctr(2)) + dt*((0.5-beta)*nacc_old_si(esctr(2))+beta*nacc_si(esctr(2))));
    x_sp(spid) = x_sp(spid) + dt * N0 * (nmomentum_si(esctr(1)-1)/nmass_si(esctr(1)-1) + dt*((0.5-beta)*nacc_old_si(esctr(1)-1)+beta*nacc_si(esctr(1)-1)));
    
    else
    v_ssp(spid) = v_ssp(spid) + dt * N1 * ((1-gamma)*nacc_old_si(esctr(1))+gamma*nacc_si(esctr(1)) - damps * nmomentum_si(esctr(1))/nmass_si(esctr(1))); 
    v_ssp(spid) = v_ssp(spid) + dt * N2 * ((1-gamma)*nacc_old_si(esctr(2))+gamma*nacc_si(esctr(2)) - damps * nmomentum_si(esctr(2))/nmass_si(esctr(2)));
    v_ssp(spid) = v_ssp(spid) + dt * N0 * ((1-gamma)*nacc_old_si(esctr(1)-1)+gamma*nacc_si(esctr(1)-1) - damps * nmomentum_si(esctr(1)-1)/nmass_si(esctr(1)-1));
    v_ssp(spid) = v_ssp(spid) + dt * N3 * ((1-gamma)*nacc_old_si(esctr(2)+1)+gamma*nacc_si(esctr(2)+1) - damps * nmomentum_si(esctr(2)+1)/nmass_si(esctr(2)+1));

    x_sp(spid) = x_sp(spid) + dt * N1 * (nmomentum_si(esctr(1))/nmass_si(esctr(1)) + dt*((0.5-beta)*nacc_old_si(esctr(1))+beta*nacc_si(esctr(1))));
    x_sp(spid) = x_sp(spid) + dt * N2 * (nmomentum_si(esctr(2))/nmass_si(esctr(2)) + dt*((0.5-beta)*nacc_old_si(esctr(2))+beta*nacc_si(esctr(2))));
    x_sp(spid) = x_sp(spid) + dt * N0 * (nmomentum_si(esctr(1)-1)/nmass_si(esctr(1)-1) + dt*((0.5-beta)*nacc_old_si(esctr(1)-1)+beta*nacc_si(esctr(1)-1)));
    x_sp(spid) = x_sp(spid) + dt * N3 * (nmomentum_si(esctr(2)+1)/nmass_si(esctr(2)+1) + dt*((0.5-beta)*nacc_old_si(esctr(2)+1)+beta*nacc_si(esctr(2)+1)));
    end
end

% particle (updated) velocities are mapped back to nodes
    nvelo_si(esctr(1)-1)    = nvelo_si(esctr(1)-1)+ N0 * m_sp(spid) * v_ssp(spid);
    nvelo_si(esctr(1))      = nvelo_si(esctr(1))+ N1 * m_sp(spid) * v_ssp(spid);
    nvelo_si(esctr(2))      = nvelo_si(esctr(2))+ N2 * m_sp(spid) * v_ssp(spid);
    nvelo_si(esctr(2)+1)    = nvelo_si(esctr(2)+1)+ N3 * m_sp(spid) * v_ssp(spid);
    end
    end
    
%% Store the particles into cell for solid phase
mspoints=cell(ncell,1);                                     % matrix to store particles for each cells

% define the position of particle in which cell
for sp=1:spCount
x = x_sp(sp);
c = (floor(x/dcell)+1);
spElems(sp) = c;                                            % Particle p stay in element c
end

% loop over cells and store particles in element "e"
for c=1:ncell
    id_sp = find(spElems==c);
    mspoints{c} = id_sp;                                    % mspoints {e} indicates particles in c
end

%% compute nodal velocities
    for n = snode_surface:nodeCount-2
        if nmass_si(n)==0
            break
        else
        nvelo_si(n) = nvelo_si(n)/nmass_si(n);
        end
    end
    
% boundary conditions
if bound==1
    nvelo_si(nodeCount) = 0;  
    nvelo_si(nodeCount-1) = 0;  
    nvelo_si(nodeCount-2) = 0;
end

    % Compute A_si
    for n = snode_surface:nodeCount-2
    if n==snode_surface
    A_si(n) = (nvelo_si(n+1) - nvelo_si(n))/2/nvolume_si(n);
    elseif n==nodeCount-2
    A_si(n) = (nvelo_si(n) - nvelo_si(n-1))/2/nvolume_si(n);
    else
    A_si(n) = (nvelo_si(n+1) - nvelo_si(n-1))/2/nvolume_si(n);
    end
    end
    
%% Update liquid particle velocity and position
     for c = wnode_surface:ncell-2
        esctr = cells(c,:);          
        enode = nodes(esctr);                 
        mwpts = mwpoints{c};              

% loop over particles
    for wp=1:length (mwpts)
        wpid = mwpts(wp);                  % loop each particle (spid) inside an element
        
% compute shape functions N1, N2
    if choice ==1
    [N1,N2,dN1,dN2]=linearshape(x_wp(wpid),enode(1),enode(2));
    N0=0;dN0=0;N3=0;dN3=0;
    elseif choice ==2
    [N0,dN0]=GIMPshape(x_wp(wpid),enode(1)-dcell,dcell,dparticlew);
    [N1,dN1]=GIMPshape(x_wp(wpid),enode(1),dcell,dparticlew);
    [N2,dN2]=GIMPshape(x_wp(wpid),enode(2),dcell,dparticlew);
    [N3,dN3]=GIMPshape(x_wp(wpid),enode(2)+dcell,dcell,dparticlew);
    elseif choice ==3
    [N0,dN0]=GIMPshape(x_wp(wpid),enode(1)-dcell,dcell,V_wp(wpid));
    [N1,dN1]=GIMPshape(x_wp(wpid),enode(1),dcell,V_wp(wpid));
    [N2,dN2]=GIMPshape(x_wp(wpid),enode(2),dcell,V_wp(wpid));
    [N3,dN3]=GIMPshape(x_wp(wpid),enode(2)+dcell,dcell,V_wp(wpid));
    elseif choice ==4 || choice ==5
    [N1,N2,dN1,dN2,falpha]=DDMPshape(x_wp(wpid),enode(1),enode(2));
    N0=0;dN0=0;N3=0;dN3=0;
    end
    
%Update particle velocity and position
if TimeInte==1
    if esctr(1)==wnode_surface
    v_wp(wpid) = v_wp(wpid) + dt * N1 * (nforce_wi(esctr(1))/nmass_wi(esctr(1)) - dampw * nmomentum_wi(esctr(1))/nmass_wi(esctr(1)));
    v_wp(wpid) = v_wp(wpid) + dt * N2 * (nforce_wi(esctr(2))/nmass_wi(esctr(2)) - dampw * nmomentum_wi(esctr(2))/nmass_wi(esctr(2)));
    v_wp(wpid) = v_wp(wpid) + dt * N3 * (nforce_wi(esctr(2)+1)/nmass_wi(esctr(2)+1) - dampw * nmomentum_wi(esctr(2)+1)/nmass_wi(esctr(2)+1));
    
    x_wp(wpid) = x_wp(wpid) + dt * N1 * nmomentum_wi(esctr(1))/nmass_wi(esctr(1));
    x_wp(wpid) = x_wp(wpid) + dt * N2 * nmomentum_wi(esctr(2))/nmass_wi(esctr(2));
    x_wp(wpid) = x_wp(wpid) + dt * N3 * nmomentum_wi(esctr(2)+1)/nmass_wi(esctr(2)+1);   
     
    elseif esctr(2)==nodeCount-2
    v_wp(wpid) = v_wp(wpid) + dt * N1 * (nforce_wi(esctr(1))/nmass_wi(esctr(1)) - dampw * nmomentum_wi(esctr(1))/nmass_wi(esctr(1)));
    v_wp(wpid) = v_wp(wpid) + dt * N2 * (nforce_wi(esctr(2))/nmass_wi(esctr(2)) - dampw * nmomentum_wi(esctr(2))/nmass_wi(esctr(2)));
    v_wp(wpid) = v_wp(wpid) + dt * N0 * (nforce_wi(esctr(1)-1)/nmass_wi(esctr(1)-1) - dampw * nmomentum_wi(esctr(1)-1)/nmass_wi(esctr(1)-1));
    
    x_wp(wpid) = x_wp(wpid) + dt * N1 * nmomentum_wi(esctr(1))/nmass_wi(esctr(1));
    x_wp(wpid) = x_wp(wpid) + dt * N2 * nmomentum_wi(esctr(2))/nmass_wi(esctr(2));
    x_wp(wpid) = x_wp(wpid) + dt * N0 * nmomentum_wi(esctr(1)-1)/nmass_wi(esctr(1)-1);   
    
    else
    v_wp(wpid) = v_wp(wpid) + dt * N1 * (nforce_wi(esctr(1))/nmass_wi(esctr(1)) - dampw * nmomentum_wi(esctr(1))/nmass_wi(esctr(1)));
    v_wp(wpid) = v_wp(wpid) + dt * N2 * (nforce_wi(esctr(2))/nmass_wi(esctr(2)) - dampw * nmomentum_wi(esctr(2))/nmass_wi(esctr(2)));
    v_wp(wpid) = v_wp(wpid) + dt * N0 * (nforce_wi(esctr(1)-1)/nmass_wi(esctr(1)-1) - dampw * nmomentum_wi(esctr(1)-1)/nmass_wi(esctr(1)-1));
    v_wp(wpid) = v_wp(wpid) + dt * N3 * (nforce_wi(esctr(2)+1)/nmass_wi(esctr(2)+1) - dampw * nmomentum_wi(esctr(2)+1)/nmass_wi(esctr(2)+1));
    
    x_wp(wpid) = x_wp(wpid) + dt * N1 * nmomentum_wi(esctr(1))/nmass_wi(esctr(1));
    x_wp(wpid) = x_wp(wpid) + dt * N2 * nmomentum_wi(esctr(2))/nmass_wi(esctr(2));
    x_wp(wpid) = x_wp(wpid) + dt * N0 * nmomentum_wi(esctr(1)-1)/nmass_wi(esctr(1)-1);
    x_wp(wpid) = x_wp(wpid) + dt * N3 * nmomentum_wi(esctr(2)+1)/nmass_wi(esctr(2)+1);   
    end
    
elseif TimeInte==2
    if esctr(1)==wnode_surface
    v_wp(wpid) = v_wp(wpid) + dt * N1 * ((1-gamma)*nacc_old_wi(esctr(1))+gamma*nacc_wi(esctr(1)) - dampw * nmomentum_wi(esctr(1))/nmass_wi(esctr(1)));
    v_wp(wpid) = v_wp(wpid) + dt * N2 * ((1-gamma)*nacc_old_wi(esctr(2))+gamma*nacc_wi(esctr(2)) - dampw * nmomentum_wi(esctr(2))/nmass_wi(esctr(2)));
    v_wp(wpid) = v_wp(wpid) + dt * N3 * ((1-gamma)*nacc_old_wi(esctr(2)+1)+gamma*nacc_wi(esctr(2)+1) - dampw * nmomentum_wi(esctr(2)+1)/nmass_wi(esctr(2)+1));
    
    x_wp(wpid) = x_wp(wpid) + dt * N1 * (nmomentum_wi(esctr(1))/nmass_wi(esctr(1))+dt*((0.5-beta)*nacc_old_wi(esctr(1))+beta*nacc_wi(esctr(1))));
    x_wp(wpid) = x_wp(wpid) + dt * N2 * (nmomentum_wi(esctr(2))/nmass_wi(esctr(2))+dt*((0.5-beta)*nacc_old_wi(esctr(2))+beta*nacc_wi(esctr(2))));
    x_wp(wpid) = x_wp(wpid) + dt * N3 * (nmomentum_wi(esctr(2)+1)/nmass_wi(esctr(2)+1)+dt*((0.5-beta)*nacc_old_wi(esctr(2)+1)+beta*nacc_wi(esctr(2)+1)));   
     
    elseif esctr(2)==nodeCount-2
    v_wp(wpid) = v_wp(wpid) + dt * N1 * ((1-gamma)*nacc_old_wi(esctr(1))+gamma*nacc_wi(esctr(1)) - dampw * nmomentum_wi(esctr(1))/nmass_wi(esctr(1)));
    v_wp(wpid) = v_wp(wpid) + dt * N2 * ((1-gamma)*nacc_old_wi(esctr(2))+gamma*nacc_wi(esctr(2)) - dampw * nmomentum_wi(esctr(2))/nmass_wi(esctr(2)));
    v_wp(wpid) = v_wp(wpid) + dt * N0 * ((1-gamma)*nacc_old_wi(esctr(1)-1)+gamma*nacc_wi(esctr(1)-1) - dampw * nmomentum_wi(esctr(1)-1)/nmass_wi(esctr(1)-1));
    
    x_wp(wpid) = x_wp(wpid) + dt * N1 * (nmomentum_wi(esctr(1))/nmass_wi(esctr(1))+dt*((0.5-beta)*nacc_old_wi(esctr(1))+beta*nacc_wi(esctr(1))));
    x_wp(wpid) = x_wp(wpid) + dt * N2 * (nmomentum_wi(esctr(2))/nmass_wi(esctr(2))+dt*((0.5-beta)*nacc_old_wi(esctr(2))+beta*nacc_wi(esctr(2))));
    x_wp(wpid) = x_wp(wpid) + dt * N0 * (nmomentum_wi(esctr(1)-1)/nmass_wi(esctr(1)-1)+dt*((0.5-beta)*nacc_old_wi(esctr(1)-1)+beta*nacc_wi(esctr(1)-1)));   
    
    else
    v_wp(wpid) = v_wp(wpid) + dt * N1 * ((1-gamma)*nacc_old_wi(esctr(1))+gamma*nacc_wi(esctr(1)) - dampw * nmomentum_wi(esctr(1))/nmass_wi(esctr(1)));
    v_wp(wpid) = v_wp(wpid) + dt * N2 * ((1-gamma)*nacc_old_wi(esctr(2))+gamma*nacc_wi(esctr(2)) - dampw * nmomentum_wi(esctr(2))/nmass_wi(esctr(2)));
    v_wp(wpid) = v_wp(wpid) + dt * N0 * ((1-gamma)*nacc_old_wi(esctr(1)-1)+gamma*nacc_wi(esctr(1)-1) - dampw * nmomentum_wi(esctr(1)-1)/nmass_wi(esctr(1)-1));
    v_wp(wpid) = v_wp(wpid) + dt * N3 * ((1-gamma)*nacc_old_wi(esctr(2)+1)+gamma*nacc_wi(esctr(2)+1) - dampw * nmomentum_wi(esctr(2)+1)/nmass_wi(esctr(2)+1));
    
    x_wp(wpid) = x_wp(wpid) + dt * N1 * (nmomentum_wi(esctr(1))/nmass_wi(esctr(1))+dt*((0.5-beta)*nacc_old_wi(esctr(1))+beta*nacc_wi(esctr(1))));
    x_wp(wpid) = x_wp(wpid) + dt * N2 * (nmomentum_wi(esctr(2))/nmass_wi(esctr(2))+dt*((0.5-beta)*nacc_old_wi(esctr(2))+beta*nacc_wi(esctr(2))));
    x_wp(wpid) = x_wp(wpid) + dt * N0 * (nmomentum_wi(esctr(1)-1)/nmass_wi(esctr(1)-1)+dt*((0.5-beta)*nacc_old_wi(esctr(1)-1)+beta*nacc_wi(esctr(1)-1)));
    x_wp(wpid) = x_wp(wpid) + dt * N3 * (nmomentum_wi(esctr(2)+1)/nmass_wi(esctr(2)+1)+dt*((0.5-beta)*nacc_old_wi(esctr(2)+1)+beta*nacc_wi(esctr(2)+1)));   
    end
end
% particle (updated) velocities are mapped back to nodes
    nvelo_wi(esctr(1)-1)    = nvelo_wi(esctr(1)-1)+ N0 * m_wp(wpid) * v_wp(wpid);
    nvelo_wi(esctr(1))      = nvelo_wi(esctr(1))+ N1 * m_wp(wpid) * v_wp(wpid);
    nvelo_wi(esctr(2))      = nvelo_wi(esctr(2))+ N2 * m_wp(wpid) * v_wp(wpid);
    nvelo_wi(esctr(2)+1)      = nvelo_wi(esctr(2)+1)+ N3 * m_wp(wpid) * v_wp(wpid);
    end
     end
     
%% Store the particles into cell for liquid phase
mwpoints=cell(ncell,1);                                     % matrix to store particles for each cells

% define the position of particle in which cell
for wp=1:wpCount
x = x_wp(wp);
c = (floor(x/dcell)+1);
wpElems(wp) = c;                                            % Particle p stay in element c
end

% loop over cells and store particles in element "e"
for c=1:ncell
    id_wp = find(wpElems==c);
    mwpoints{c} = id_wp;                                    % mwpoints {e} indicates particles in c
end

     %% compute nodal velocities
    for n = snode_surface:nodeCount-2
        if nmass_wi(n)==0
            break
        else
            nvelo_wi(n) = nvelo_wi(n)/nmass_wi(n);
        end
    end
    
% boundary conditions
if bound==1
    nvelo_wi(nodeCount) = 0;  
    nvelo_wi(nodeCount-1) = 0;  
    nvelo_wi(nodeCount-2) = 0;  
end

    % Compute A_wi
    for n = wnode_surface:nodeCount-2
    if n==wnode_surface
    A_wi(n) = (nvelo_wi(n+1) - nvelo_wi(n))/2/nvolume_wi(n);
    elseif n==nodeCount-2
    A_wi(n) = (nvelo_wi(n) - nvelo_wi(n-1))/2/nvolume_wi(n);
    else
    A_wi(n) = (nvelo_wi(n+1) - nvelo_wi(n-1))/2/nvolume_wi(n);
    end
    end
    
%% Update effective stress
    for c = snode_surface:ncell-2
    esctr = cells(c,:);
    enode = nodes(esctr);
    mspts = mspoints{c};
    
    for sp=1:length(mspts)                % loop over particles
    spid = mspts(sp);
    
% compute shape functions N1, N2
    if choice ==1
    [N1,N2,dN1,dN2]=linearshape(x_sp(spid),enode(1),enode(2));
     N0=0;dN0=0;N3=0;dN3=0;
    elseif choice ==2
    [N0,dN0]=GIMPshape(x_sp(spid),enode(1)-dcell,dcell,dparticles);        
    [N1,dN1]=GIMPshape(x_sp(spid),enode(1),dcell,dparticles);
    [N2,dN2]=GIMPshape(x_sp(spid),enode(2),dcell,dparticles);
    [N3,dN3]=GIMPshape(x_sp(spid),enode(2)+dcell,dcell,dparticles);
    elseif choice ==3
    [N0,dN0]=GIMPshape(x_sp(spid),enode(1)-dcell,dcell,V_sp(spid));        
    [N1,dN1]=GIMPshape(x_sp(spid),enode(1),dcell,V_sp(spid));
    [N2,dN2]=GIMPshape(x_sp(spid),enode(2),dcell,V_sp(spid));
    [N3,dN3]=GIMPshape(x_sp(spid),enode(2)+dcell,dcell,V_sp(spid));    
    elseif choice ==4 || choice ==5
    [N1,N2,dN1,dN2,falpha]=DDMPshape(x_sp(spid),enode(1),enode(2));
    N0=0;dN0=0;N3=0;dN3=0;
    end
       
    if choice ==4 || choice ==5
    L_sp1(spid) = dN1 * nvelo_si(esctr(1)) + dN2 * nvelo_si(esctr(2)) + dN0 * nvelo_si(esctr(1)-1) + dN3 * nvelo_si(esctr(2)+1) ; 
    L_sp2(spid) = N1 * A_si(esctr(1)) + N2 * A_si(esctr(2)) + N0 * A_si(esctr(1)-1) + N3 * A_si(esctr(2)+1) ;
    L_sp(spid)  = falpha * L_sp1(spid) + (1 - falpha) * L_sp2(spid);   % gradient velocity
    else
    L_sp(spid) = dN1 * nvelo_si(esctr(1)) + dN2 * nvelo_si(esctr(2)) + dN0 * nvelo_si(esctr(1)-1) + dN3 * nvelo_si(esctr(2)+1) ;   % gradient velocity
    end
    
    F_sp(spid) = (1 + L_sp(spid) * dt) * F_sp (spid);                   % deformation tensor
    V_sp(spid) = F_sp (spid) * V_spo(spid);                             % update volume of particle
    dEps(spid) = dt * L_sp(spid);                                             % strain rate
    n_sp(spid) =  1 - (1 - n_o)/F_sp(spid);                             % porousity
    k_sp(spid) = ((1-n_spo(spid))/(1-n_sp(spid))).^2*k_spo(spid);
    end
    end
    
    %% Filter the gradient velocity
   % Build matrix for nullspace filter
    if choice ==5
    for c = snode_surface:ncell-2
    esctr = cells(c,:);
    enode = nodes(esctr);
    mspts = mspoints{c};
    
    for sp=1:length(mspts)                % loop over particles
    spid = mspts(sp);
    [N1,N2,dN1,dN2,falpha]=DDMPshape(x_sp(spid),enode(1),enode(2));
    N0=0;dN0=0;N3=0;dN3=0;
    
    dN_si_matrix(esctr(1),spid) = dN1;
    dN_si_matrix(esctr(2),spid) = dN2;
    end
    end
      
    % Filter
   [dEps,~] = filter1(dEps,dN_si_matrix);
    end
    
    %% Update effective stress
    for sp=1:spCount
        	s_sp(sp) = s_sp(sp) + E * dEps(sp);                                 % stress
    end
    
%     s_sp(1)=0;
   
    %% Update pore pressure
   for c = wnode_surface:ncell-2
    esctr = cells(c,:);
    enode = nodes(esctr);
    mwpts = mwpoints{c};
    
    for wp=1:length(mwpts)                % loop over particles
    wpid = mwpts(wp);
    
% compute shape functions N1, N2
  
    if choice ==1
    [N1,N2,dN1,dN2]=linearshape(x_wp(wpid),enode(1),enode(2));
    [~,~,dN1_c,dN2_c]=linearshape(enode(1)+(enode(2)-enode(1))/2,enode(1),enode(2));
    N0=0;dN0=0;N3=0;dN3=0;
    dN0_c=0;dN3_c=0;
    elseif choice ==2
    [N0,dN0]=GIMPshape(x_wp(wpid),enode(1)-dcell,dcell,dparticlew);
    [N1,dN1]=GIMPshape(x_wp(wpid),enode(1),dcell,dparticlew);
    [N2,dN2]=GIMPshape(x_wp(wpid),enode(2),dcell,dparticlew);
    [N3,dN3]=GIMPshape(x_wp(wpid),enode(2)+dcell,dcell,dparticlew);
    [~,dN0_c]=GIMPshape(enode(1)+(enode(2)-enode(1))/2,enode(1)-dcell,dcell,dparticlew);
    [~,dN1_c]=GIMPshape(enode(1)+(enode(2)-enode(1))/2,enode(1),dcell,dparticlew);
    [~,dN2_c]=GIMPshape(enode(1)+(enode(2)-enode(1))/2,enode(2),dcell,dparticlew);
    [~,dN3_c]=GIMPshape(enode(1)+(enode(2)-enode(1))/2,enode(2)+dcell,dcell,dparticlew);
    elseif choice ==3
    [N0,dN0]=GIMPshape(x_wp(wpid),enode(1)-dcell,dcell,V_wp(wpid));
    [N1,dN1]=GIMPshape(x_wp(wpid),enode(1),dcell,V_wp(wpid));
    [N2,dN2]=GIMPshape(x_wp(wpid),enode(2),dcell,V_wp(wpid));
    [N3,dN3]=GIMPshape(x_wp(wpid),enode(2)+dcell,dcell,V_wp(wpid));
    [~,dN0_c]=GIMPshape(enode(1)+(enode(2)-enode(1))/2,enode(1)-dcell,dcell,V_wp(wpid));
    [~,dN1_c]=GIMPshape(enode(1)+(enode(2)-enode(1))/2,enode(1),dcell,V_wp(wpid));
    [~,dN2_c]=GIMPshape(enode(1)+(enode(2)-enode(1))/2,enode(2),dcell,V_wp(wpid));
    [~,dN3_c]=GIMPshape(enode(1)+(enode(2)-enode(1))/2,enode(2)+dcell,dcell,V_wp(wpid));
    elseif choice ==4 || choice ==5
    [N1,N2,dN1,dN2,~]=DDMPshape(x_wp(wpid),enode(1),enode(2));
    N0=0;dN0=0;N3=0;dN3=0;
    [~,~,dN1_c,dN2_c,falpha]=DDMPshape(enode(1)+(enode(2)-enode(1))/2,enode(1),enode(2));
    dN0_c=0;dN3_c=0;
    end

    if choice ==4 || choice ==5
    L_swp1(wpid)= dN1_c * nvelo_si(esctr(1)) + dN2_c * nvelo_si(esctr(2)) + dN0_c * nvelo_si(esctr(1)-1) + dN3_c * nvelo_si(esctr(2)+1); 
    L_wp1(wpid) = dN1_c * nvelo_wi(esctr(1)) + dN2_c * nvelo_wi(esctr(2)) + dN0_c * nvelo_wi(esctr(1)-1) + dN3_c * nvelo_wi(esctr(2)+1);   
    
    L_swp2(wpid)= N1_c * A_si(esctr(1)) + N2_c * A_si(esctr(2)) + N0_c * A_si(esctr(1)-1) + N3_c * A_si(esctr(2)+1); 
    L_wp2(wpid) = N1_c * A_wi(esctr(1)) + N2_c * A_wi(esctr(2)) + N0_c * A_wi(esctr(1)-1) + N3_c * A_wi(esctr(2)+1);   
    
    L_swp(wpid)= falpha * L_swp1(wpid) + (1 - falpha) * L_swp2(wpid);   % gradient velocity
    L_wp(wpid)  = falpha * L_wp1(wpid) + (1 - falpha) * L_wp2(wpid);   % gradient velocity
    else
    L_swp(wpid) = dN1_c * nvelo_si(esctr(1)) + dN2_c * nvelo_si(esctr(2)) + dN0_c * nvelo_si(esctr(1)-1) + dN3_c * nvelo_si(esctr(2)+1); % gradient velocity of solid
    L_wp(wpid)  = dN1_c * nvelo_wi(esctr(1)) + dN2_c * nvelo_wi(esctr(2)) + dN0_c * nvelo_wi(esctr(1)-1) + dN3_c * nvelo_wi(esctr(2)+1);  % gradient velocity of liquid
    end
    
    de_swp(wpid)= dt/n_wp(wpid) * (-n_wp(wpid)) * L_swp(wpid);
    de_wp(wpid)= dt/n_wp(wpid) * (1-n_wp(wpid)) * L_wp(wpid);
    de_wwp(wpid) = de_wp(wpid) + de_swp(wpid);
    end
   end

%% Filter the gradient velocity
   % Build matrix for nullspace filter
    if choice ==5
    for c = wnode_surface:ncell-2
    esctr = cells(c,:);
    enode = nodes(esctr);
    mwpts = mwpoints{c};
     
    for wp=1:length(mwpts)                % loop over particles
    wpid = mwpts(wp);
    [N1,N2,dN1,dN2,falpha]=DDMPshape(x_wp(wpid),enode(1),enode(2));
    N0=0;dN0=0;N3=0;dN3=0;
    
    dN_wi_matrix(esctr(1),wpid) = dN1;
    dN_wi_matrix(esctr(2),wpid) = dN2;
    end
    end
      
    % Filter
   [de_wwp,~] = filter1(de_wwp,dN_wi_matrix);
    end
   
    % Update pore water pressure
    for wp=1:wpCount               
    pore_wp(wp) = pore_wp(wp) + Kw*de_wwp(wp);
    end

      pore_wp(1)=0;
   
%% Save accerleration
if TimeInte==2
    nacc_old_si=nacc_si;
    nacc_old_wi=nacc_wi;
end

    %% next step
    t=t+dt;    

 end

 %% Analytical model
    pore_wp_ana(:)=0;
    s_sp_ana(:)=0;
    
  sp_last=1;
  
 for sp=1:sp_last
     s_sp_ana(sp)=(x_sp(sp)-x_sp(1))*g*psp*(1-n_o);
 end
 
for sp=sp_last+1:spCount
     s_sp_ana(sp)=s_sp_ana(sp_last)+(x_sp(sp)-x_sp(sp_last))*g*((1-n_o)*psp + n_o * pwp - pwp);
 end


 for sp=1:spCount
     s_sp_ana(sp)=(x_sp(sp)-x_sp(1))*g*((1-n_o)*psp + n_o * pwp - pwp);
 end
 
 for wp=1:wpCount
     pore_wp_ana(wp)=(x_wp(wp)-x_wp(1))*g*pwp;
 end
 
 %% Figure
      for sp=1:spCount
        x_sp(sp)=x_sp(sp)-2*dcell;
    end
    
    for wp=1:wpCount
        x_wp(wp)=x_wp(wp)-2*dcell;
    end

    % Store Tv
       mv =	1/E;
       Sp=0.3*(1/Kw)+0.7/E;
       k_ana = mean(k_sp);
    
       Cv = k_ana/(mv+Sp)/pwp;
       Tv=Cv*t;
       
       Us=0;
     for i=1:100
     M_i = (i-0.5)*pi;
     Us = Us + + 2/M_i/M_i*exp(-M_i*M_i*Tv);
     end
     t;
     Us=1-Us        
       time = [time Tv];        
    % Store settlement
    Set=0;
     for i=1:100
     M_i = (i-0.5)*pi;
         Set = Set + 2/M_i/M_i*exp(-M_i*M_i*Tv);
     end
    
    S_final = (L-4*dcell)*(1-exp(-(mv)*ppp));
        check = [check (x_sp(1))];
   
    Set = S_final*(1-Set);
    Set_ana = [Set_ana Set];
    
    sVelocity1 = [sVelocity1 nvelo_si(snode_surface)];
    wVelocity1 = [wVelocity1 -nvelo_wi(wnode_surface)-nvelo_si(snode_surface)];
    
     sVelocity1_ana=0;
     for i=1:100
     M_i = (i-0.5)*pi;
         sVelocity1_ana = sVelocity1_ana + exp(-M_i*M_i*Tv);
     end
     sVelocity1_ana = S_final/(L-4*dcell)/(L-4*dcell)*2*Cv*sVelocity1_ana;
     
     % Void ratio
     e_final = 1/(1-n_o)*exp(-(mv)*ppp)-1;
     e_o = n_o/(1-n_o);
     
     e_ana = 0;
     for i=1:100
     M_i = (i-0.5)*pi;
         e_ana = e_ana + 2/M_i*sin(0)*exp(-M_i*M_i*Tv);
     end
     
     e_ana = e_final + (e_o - e_final)*e_ana;
    
     wVelocity1_ana=-sVelocity1_ana*(1-n_o)/n_o;
     sVelocity1_ana1 = [sVelocity1_ana1 sVelocity1_ana];
     wVelocity1_ana1 = [wVelocity1_ana1 wVelocity1_ana];
     
    % effective and pore water pressure
 for i=1:100
     M_i = (i-0.5)*pi;
     for wp=1:wpCount
         pore_wp_ana(wp) = pore_wp_ana(wp) + 2/M_i*sin(M_i*(x_wp(wp)-x_wp(1)+dparticlew/2)/(L-4*dcell-x_wp(1)+dparticlew/2))*exp(-M_i*M_i*Tv);
     end
 end
 
 for wp=1:wpCount
 pore_wp_ana(wp)=1/(mv+Sp)  *  (log(1+(exp((mv+Sp)*ppp)-1)*pore_wp_ana(wp))/log(exp(1)));
 end
  
  for i=1:100
     for sp=1:spCount
         s_sp_ana(sp) = s_sp_ana(sp) + (ppp - (1/(mv+Sp)*log(1+(exp((mv+Sp)*ppp)-1)*2/M_i*sin(M_i*(x_sp(sp)-x_sp(1)+dparticles/2)/(L-4*dcell-x_sp(1)+dparticles/2))*exp(-M_i*M_i*Tv))/log(exp(1))));
     end
  end
    
  % Figure of settlement
    Settlement = figure;
    set(Settlement, 'visible','off');
    plot(time,check,'o',time,Set_ana);

    
  % Figure of velocity
    Velocityws = figure;
    set(Velocityws, 'visible','off');
    plot(time,sVelocity1,'o',time,sVelocity1_ana1,time,wVelocity1,'x',time,wVelocity1_ana1);
    legend('sVelocity1','sVelocity1_ana1','wVelocity1','wVelocity1_ana1','Location','northeast')


    StressProfile = figure;
    set(StressProfile, 'visible','off');
    if bound==1
    plot(-pore_wp,-x_wp,'o',pore_wp_ana,-x_wp);
    elseif bound==2
    plot(-pore_wp,-x_wp,'o',pore_wp_ana,-x_wp/2);
    end
    if choice==1
    str=sprintf('Stress plot of MPM solution in 1D at time (second): %d',ft);
    title(str);
    elseif choice ==2
     str=sprintf('Stress plot of uGIMP solution in 1D at time (second): %d',ft);
         title(str);
    elseif choice ==3
    str=sprintf('Stress plot of cpGIMP solution in 1D at time (second): %d',ft);
        title(str);
    elseif choice ==4
    str=sprintf('Stress plot of DDMP solution in 1D at time (second): %d',ft);
        title(str);
    elseif choice ==5
    str=sprintf('Stress plot of DDMP solution in 1D with null space filter at time (second): %d',ft);
        title(str);
    end
    xlabel('Stress (kPa)'); % label for y axis
    ylabel('position (m)'); % label for x axis
    legend('pore pressure','analytical pore pressure','Location','northeast')
    axis([0 ppp*1.25 -1.02 0]);
    
    poro=figure;
    plot(n_sp,-x_sp);
    set(poro, 'visible','off');

    if choice==1
    title('porosity plot of MPM solution in 1D'); % title;
    elseif choice ==2
    title('porosity plot of uGIMP solution in 1D'); % title;
    elseif choice ==3
    title('porosity plot of cpGIMP solution in 1D'); % title;
    elseif choice ==4
    title('porosity plot of DDMP solution in 1D'); % title;
    elseif choice ==5
    title('porosity plot of DDMP solution with null space filter in 1D'); % title;
    end
    xlabel('Porosity'); % label for y axis
    ylabel('position (m)'); % label for x axis
    legend('Soil Porosity','Location','northwest')
    axis([0.1 0.3 -L 0])

    for sp=1:spCount
        x_sp(sp)=x_sp(sp)+2*dcell;
    end
    
    for wp=1:wpCount
        x_wp(wp)=x_wp(wp)+2*dcell;
    end


    frame3 = getframe(StressProfile);
    writeVideo(writerObj3,frame3);
    end
    close(writerObj3);
    toc